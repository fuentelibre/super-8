
# Jappy Super-8 Fantasy Console

Super-8 is a prototype code learning environment designed for creating interactive art and videogames.

It is a "live" coding environment, with instant feedback for the user.

It is heavily influenced by [Script-8](https://script-8.github.io/) by Gabriel Florit.

In fact, all of the API and drawing engine is directly taken from Script-8. This makes Super-8 games "almost" compatible with Script-8 games.

There are a few differences:

  - 16 vivid colors
  - 200x160 resolution
  - Overlay layer with p5js graphics API (e.g. for effects)
  - Able to run as server or stand-alone offline application (needs Chromium based browser installed)
  - Protection from Infinite Loops

Super-8 is work in progress. Missing (planned) features:

  - Ability to publish and share games (partially implemented)
  - Standalone game exporter
  - i18n
  - Music/SFX editor
  - Low priority: Number slider, time travel, other debugging options

# Credits

Super-8 by Sebastian Silva (*icarito*).

Special thanks to Gabriel Florit and Keith Simmons.

Code taken from Script-8:

 - frameBufferCanvasAPI
 - trace.js

Code taken from CodePen:

 - InfiniteLoopBuster

*sweetie-16* color palette by GrafxKid.
