#!/bin/env python3
""" SUPER 8 Fantasy Console
Copyright (c) Sebastian Silva - sebastian@fuentelibre.org

# server.py - Super-8 service launcher

This software is under the AGPL-3 license.
"""

import os
import argparse
import sys
from super8 import Super8

parser = argparse.ArgumentParser()
parser.add_argument("--static", help="output static build",
                     action="store_true")
parser.add_argument("--production", help="use prebuilt assets",
                     action="store_true")
args = parser.parse_args()

S8 = Super8(production=args.production)

if args.static:
    os.makedirs('./static/dist/', exist_ok=True)
    with open('./static/index.html', 'w') as opened_file:
        opened_file.write(S8.render())
    sys.exit()

PORT = os.environ.get('PORT') or 39000
S8.serve(int(PORT), open=False)
