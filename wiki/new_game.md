<div style="padding: 20px; background-color: #385dc966; text-align: center">

<span class="oi oi-bullhorn" style="font-size: 30pt" data-glyph="bullhorn"></span>

<p>Please be aware that SUPER-8 is under development and <em>things may break!</em></p>

<p>This means some features are not yet implemented and also some things may change.</p>



<p>The server administrators reserve the right to curate or even modify the games in the cartridge collection. By default, games can be modified by <em>any party</em>. Authorization and some form of version control will be implemented in the future.</p>

<p><em>By uploading your game you will be granting the server administrators the distribution rights of your game to be included in the server's <strong>Game Collection</strong> under a AGPL-3 license.</em></p>
</div>
<br>

* Games are automatically saved into your browser session storage until they are "ejected": <button disabled style="padding: 5px; border: 2px solid #363636dd; background-color: #36363666;"><span style="vertical-align: top; font-size: 1.3em" class="oi oi-eject" data-glyph="eject"></span></button>.
* Your game won't be uploaded to the server until you press "upload": <button disabled style="padding: 5px; border: 2px solid #363636dd; background-color: #36363666;"><span style="vertical-align: top; font-size: 1.3em" class="oi oi-cloud-upload" data-glyph="cloud-upload"></span></button>.
