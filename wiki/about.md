## Creating Video Games With Super-8

### Super-8 is designed to enhance productivity and joy in making videogames.

We are inspired in earlier, simpler times, when a lone visionary or small family team could produce masterpieces in the nascent art of videogames, by coding meticulously, and also *meaningfully*, to express his or her imagination into experiences, interactive stories.

While modern tools are impressive, they are also increasingly complex and have become a barrier of entry. In our childhood, computers were programable at boot time, even without an operating system installed.

Super-8 offers a robust and simple graphics API ([[Script-8]], purposely limited with the intention to promote that focus flows into **the goal**, that is, to design joyful optionally interactive experiences.

For the case of special effects or needs, we have an *overlay layer* that uses the [[p5_js]] drawing API. This also opens the door to provide more interaction such as using any key and tracking the mouse location.

* Go on to our [[tutorial]] track (work in progress)
* Super-8 is *Free Software* distributed under the terms of the [[AGPL]] v.3
  * The [[README]] offers more details and installation instructions.
  * Download the application and report issues at our [[repository]].
