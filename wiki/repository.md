The source for Super-8 is hosted at Gitlab:

[https://gitlab.com/fuentelibre/super-8/](https://gitlab.com/fuentelibre/super-8/)

Also we can use the issue tracker for [reporting issues](https://gitlab.com/fuentelibre/super-8/-/issues/new).
