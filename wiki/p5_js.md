## p5.js is a library for creative coding

> "...with a focus on making coding accessible and inclusive for artists, designers, educators, beginners, and anyone else!"

Super-8 automatically creates a p5.js drawing context and configures it for the target resolution of 200x160.

It is suggested to use the [[Script-8]] API and resort to p5.js for special effects.

The rendering engine automatically clears the p5 drawing context at the beginning of each frame and draws it as an overlay. It is possible to create transparency effects such as tinting or lighting. It also supports fonts, and give access to more advanced input methods.

* Visit [p5.js main website](https://p5js.org/). Most of the API reference is available in the information section at the bottom right of the screen, but may be more readble at their [Online Reference](https://p5js.org/reference/).
