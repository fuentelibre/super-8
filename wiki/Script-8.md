## Fantasy Console

![](/wiki/script-8_1.gif){: style="float: right; margin: 5px 5px 5px 1em" }

[*Script-8*](https://script-8.github.io/), by Gabriel Florit, is the direct ancestor of Super-8. It is a fantasy console with a live editor and, 8 color palette and 128x128 canvas at 60fps.

## Script-8 API in Super-8

Script-8 offers a simple framebuffer drawing API, that is faster than [[p5_js]] . In the case of Super&#8209;8, it is limited to 16 colors, with a resolution of 200x160 and 30 frames per second.

Except for the differences described above, the API works exactly the same as in Script&#8209;8, because in fact we have reused the drawing functions code by Keith Simmons.

This API doesn't blur the edges or otherwise blend colors in any way, providing the retro look and feel. It also offers basic input support and a handcrafted 8x4 font.

Games in Super-8 generally use this API.
