## Fantasized Assorted Questions (F.A.Q.)

- Where are my games saved?

Games are saved on the server side. If you are running your own instance, then the game files are stored in the `shelf/` directory.
