// title: aquarium
const FPS = 1000 / 30
const BUBBLE_1 = [127, 111, 95, 79, 63, 47, 31, 15]
const PLANT_1 = [119, 103, 87];

init = S => {
  S.light = 1
  S.time = 0
  S.day = 1000 * 60
  S.sand = 0.88
  S.sandColor = 1
  S.actors = [
    ...range(0, 3).map(createBubble),
    ...range(0, 16).map(createFish),
    ...range(0, 12).map(createPlant)
  ]
  S.fish = actorIndexes(S.actors, 'F')
  S.bubbles = actorIndexes(S.actors, 'B')
  S.plants = actorIndexes(S.actors, 'P')
}

function actorIndexes(actors, prefix) {
  return actors.reduce((group, a, i) => {
    if (a.id === prefix) {
      group.push(i)
    }
    return group
  }, [])
}

function createPlant() {
  return {
    id: 'P',
    sprite: PLANT_1[0],
    x: random(8, 184),
    y: 142 + random(-1, 2),
    age: Math.random(),
    flipH: Math.random() > 0.5
  }
}

function createBubble() {
  return {
    id: 'B',
    sprite: BUBBLE_1[0],
    x: random(8, 152),
    y: 140,
    speed: random(0.333, 0.5),
    brighten: -4
  }
}

function createFish() {
  const x = Math.random() * 100 + 50
  return {
    id: 'F',
    sprite: random(0, 14),
    x,
    y: 40,
    target: { x: x, y: Math.random() * 64 + 48 },
    brighten: random(-6, -1)
  }
}

update = (S, input, elapsed) => {
  S.time += elapsed / S.day
  S.dt = elapsed / S.day
  moveFish(S, elapsed)
  moveBubbles(S, elapsed)
  movePlants(S, elapsed)
}

function movePlants(S, elapsed) {
  S.plants.forEach(plantIndex => {
    const plant = S.actors[plantIndex]
    if(typeof plant.age === 'undefined') plant.age = 0;
    plant.age = clamp(plant.age + 0.25 * S.dt, 0, 1)
  })
}

function moveBubbles(S, elapsed) {
  S.bubbles.forEach(bubbleIndex => {
    const bubble = S.actors[bubbleIndex]
    const age = (140 - bubble.y) / (140 - 34)
    bubble.sprite = getSprite(BUBBLE_1, age)
    bubble.y -= (bubble.speed * elapsed) / FPS
    if (bubble.y < 34) {
      const { x, y, speed } = createBubble(S)
      bubble.y = y
      bubble.x = x
      bubble.speed = speed
    }
  })
}

function getSprite(sprites, age) {
  return sprites[Math.floor(sprites.length * age)];
}

function moveFish(S, elapsed) {
  S.fish.forEach(fishIndex => {
    const fish = S.actors[fishIndex]
    const [dx, dy] = targetDelta(fish)
    if (Math.abs(dx + dy) < 2) {
      fish.target = newTarget(S, fish)
      const [newDx, newDy] = targetDelta(fish)
      fish.flipH = newDx < 0
      return
    }
    fish.x += (clamp(dx / 100, -1, 1) * elapsed) / FPS
    fish.y += (clamp(dy / 100, -1, 1) * elapsed) / FPS
  })
}

function targetDelta(actor) {
  return coordDelta(actor.target.x, actor.target.y, actor.x, actor.y)
}

function coordDelta(x1, y1, x2, y2) {
  return [x1 - x2, y1 - y2]
}

function newTarget(S, fish) {
  return {
    x: Math.random() * 184 + 4,
    y: Math.random() * 80 + 60
  }
}

draw = S => {
  clear(14)
  map()
  drawSand(S)
  drawLight(S)
  drawPlants(S)
  drawActors(S)
  drawPlants(S, true)
  p5.noStroke()
  p5.fill("#44f5")
  p5.rect(1,34,198,118)
}

function drawSand(S) {
  if (S.sand == 0) return
  const color = S.sandColor + (S.light ? 0 : -2)
  const [spriteLeft, spriteMid, spriteRight] = [68, 69, 70]
  const center = 12.5
  const width = (S.sand * 13) | 0
  sprite((center - width - 1) * 8, 143, spriteLeft, color)
  range(center - width, center + width).forEach(x => {
    sprite(x * 8, 143, spriteMid, color)
  })
  sprite((center + width) * 8, 143, spriteRight, color)
}

function drawLight(S) {
  if (!S.light) return
  const lightSprite = 67
  range(1, 24).forEach(x => {
    sprite(x * 8, 32, lightSprite)
  })
}

function drawPlants(S, fg = false) {
  const plants = fg ? S.plants.slice(0, 8) : S.plants.slice(8);
  plants.forEach(plantIndex => {
    const plant = S.actors[plantIndex]
    const height = Math.floor(plant.age * 8);
    range(0, height).forEach(y => {
      const x = plant.x + Math.sin(plant.age + S.time * 120 + y)
      sprite(
        x,
        plant.y - 8 - y * 8,
        y === height - 1 ?
          PLANT_1[2] :
          PLANT_1[1],
        plant.brighten,
        plant.flipH
      )
    })
  });
}


function shuffle (array) {
  var i = 0
    , j = 0
    , temp = null

  for (i = array.length - 1; i > 0; i -= 1) {
    j = Math.floor(Math.random() * (i + 1))
    temp = array[i]
    array[i] = array[j]
    array[j] = temp
  }
}