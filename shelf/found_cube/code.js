// title: Found: cube

// originally by Thykka
// https://script-8.github.io/?id=2866b851dbc4f90307b7e993b85f5786

const distance = 10;

init = s => {
  s.frame = 0;
  s.cube = [
    { x: -1, y: -1, z: -1, l: [1, 2, 4] },
    { x: -1, y: -1, z:  1 },
    { x: -1, y:  1, z: -1, l: [3, 6] },
    { x: -1, y:  1, z:  1, l: [1] },
    { x:  1, y: -1, z: -1 },
    { x:  1, y: -1, z:  1, l: [1, 4] },
    { x:  1, y:  1, z: -1, l: [4] },
    { x:  1, y:  1, z:  1, l: [5, 6, 3] }
  ];
  s.camera = {
    xy: 80,
    z: 127,
    zoom: 174
  }
}

update = (s, key, dt) => {
  s.frame++;
  s.camera.z =128 + Math.cos(s.frame / 4) * 25
  s.camera.zoom = 174 - Math.cos(s.frame / 4) * -33;
  s.cube = s.cube.map(p => {
    rotateX(p, Math.sin(s.frame / 100) * 0.02)
    rotateY(p, Math.cos(s.frame / 67) * 0.02)
    rotateZ(p, Math.sin(s.frame / 133) * 0.01);
    p.screenX = projection(p.x * 24, p.z * 24, s.camera.xy + 20, s.camera.z, s.camera.zoom);
    p.screenY = projection(p.y * 24, p.z * 24, s.camera.xy, s.camera.z, s.camera.zoom);
    return p;
  });
}

draw = s => {
  clear();
  print(100 - (5 * 4), 2, 'Found Cube', 3)
  print(100 - (3 * 4), 144, 'We are', 6)
  print(100 - (3.5 * 4), 152, 'Brothers', 6)
  s.cube.forEach((point,pointIndex,cube) => {
    (point.l || []).forEach((lni) => {
      const link = cube[lni];

      range(32).forEach((_, fillIndex, fillArray) => {
        const v = fillIndex / (fillArray.length);
        const x = lerp(point.screenX, link.screenX, v)
          + Math.sin(v*12 + s.frame / 10) * 1
        const y = lerp(point.screenY, link.screenY, v)
          + Math.cos(v*12 - s.frame / 10) * 1
        const c = (v * 14 < 7 ? v * 14 : (13 - v) * 14) | 0
        setPixel(x, y, c);
      });
    });
    setPixel(point.screenX, point.screenY, 0);
    setPixel(point.screenX - 1, point.screenY + 0, 1);
    setPixel(point.screenX + 1, point.screenY + 0, 1);
    setPixel(point.screenX + 0, point.screenY + 1, 1);
    setPixel(point.screenX + 0, point.screenY - 1, 1);
    
  });
}

function rotateX(point, radians) {
   var y = point.y;
   point.y = (y * Math.cos(radians)) + (point.z * Math.sin(radians) * -1.0);
   point.z = (y * Math.sin(radians)) + (point.z * Math.cos(radians));
}

function rotateY(point, radians) {
   var x = point.x;
   point.x = (x * Math.cos(radians)) + (point.z * Math.sin(radians) * -1.0);
   point.z = (x * Math.sin(radians)) + (point.z * Math.cos(radians));
}

function rotateZ(point, radians) {
   var x = point.x;
   point.x = (x * Math.cos(radians)) + (point.y * Math.sin(radians) * -1.0);
   point.y = (x * Math.sin(radians)) + (point.y * Math.cos(radians));
}

function projection(xy, z, xyOffset, zOffset, distance) {
   return ((distance * xy) / (z - zOffset)) + xyOffset;
}

function lerp(v0, v1, t) {
  return v0 * (1 - t) + v1 * t;
}
