function init(state) {
  state.t = 0
  state.pos = 100
  state.bola = {
    x: 80,
    y: 100,
    vx: 1,
    vy: 1,
    hit: false
  }
  state.ladrillos = range(48).map(
    					(i)=>({
                              x: i % 6 * 30 + 10,
                              y: 10*Math.floor(i/6)+10,
                          	  color: i % 6 * 2 + 17
                            }))
}

function update(state, input, elapsed) {
  state.bola.hit = false
  state.t++
  state.bola.y = state.bola.y + state.bola.vy
  state.bola.x = state.bola.x + state.bola.vx
  
  if (Math.abs(state.bola.x - state.pos)<10 &&
      Math.abs(state.bola.y - 145)<3) {
    state.bola.vy = -1
  }
  
  if (state.bola.y < 0) state.bola.vy *= -1
  if (state.bola.x > 200) state.bola.vx *= -1
  if (state.bola.x < 0) state.bola.vx *= -1
  
  if (input.left && state.pos > 0) state.pos-=5;
  if (input.right && state.pos < 190) state.pos+=5;
  
  if (state.bola.y > 160) {
    state.bola.y = 100
    state.bola.x = 80
    state.bola.vx = 1
  }
  
  state.ladrillos.forEach(
    function(lad, index) {
      let dx = Math.abs(lad.x + 12 - state.bola.x)
      let dy = Math.abs(lad.y + 3 - state.bola.y)
      if ( dx < 13 && dy < 8) {
        state.ladrillos.splice(index, 1)
        if (dx > 10) state.bola.vx *= -1
        else state.bola.vy *= -1
        state.bola.hit = true
      }
    }
  )
}

function draw (state) {
  if (state.bola.hit)
    clear(9)
  else
    clear(8)
  rectFill(state.pos-5 + 2, 145 + 2, 20, 5, 0)
  rectFill(state.pos-5, 145, 20, 5, 3)
  rectStroke(state.pos-5, 145,20,5, 0)
  
  circFill(state.bola.x+4, state.bola.y+4, 3, 0)
  circFill(state.bola.x+2, state.bola.y+2, 3, 12)

  state.ladrillos.map(
    (lad)=>{
      rectFill(lad.x+2, lad.y+2, 25, 7, 0)
      rectFill(lad.x, lad.y, 25, 7, lad.color)
      rectStroke(lad.x, lad.y, 25, 7, 0)
    }
  )
}

