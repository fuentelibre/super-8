function init(state) {
  state.chick = {
    x: 20,
    y: 100,
    flip: 1,
    vx: 2,
    vy: 0
  }
}

function update(state, input, elapsed) {
  if (input.right) {
    state.chick.x++
    state.chick.flip = 1
  }
  if (input.left) {
    state.chick.x--
    state.chick.flip = 0
  }
  if (input.up) {
    state.chick.vy = -1.5
  }
  if (Math.abs(state.chick.vy) > 0) {
     state.chick.vy *= 0.9
  }
  state.chick.y += state.chick.vy
  if (getPixel(state.chick.x+4, state.chick.y+8)==9) {
    state.chick.vy += 0.2
  } 
  else {
    state.chick.vy = 0
  }
}

function draw (state) {
  clear(9)
  
  // Terrain
  rectFill(0, 108, 200, 52, 7)
  line(0,108,200,108,6)
  
  // Chicken
  sprite(state.chick.x, state.chick.y, 0, 0, 
         state.chick.flip)
  
  // Clouds
  sprite(130, 40, 14)
  sprite(150, 30, 14)
  sprite(110, 20, 15)
  
  // Baloon man
  sprite(160, 89, 5)
  sprite(160, 97, 21)
  sprite(160, 105, 37)
}
