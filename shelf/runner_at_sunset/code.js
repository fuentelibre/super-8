// title: Runner at Sunset #2

init = (state) => {
  state.x = 56
  state.y = 0
  state.mag = true
  state.frame = 0
  state.land = {}
  state.alt = 0
}

const seaLevel = 150

update = (state, input, elapsed) => {
  if (state.frame==0)
    generateLand(state)
  state.frame += 1
  state.animFrame = Math.floor(state.frame / 15) % 2
  let step = 30 - (state.frame % 15)
  state.flip = input.left
  state.speed = input.left || input.right ? 32 : 0
  if (state.speed > 0)
    state.x = state.x + step/15 *
              (input.left ? -state.speed/45 : state.speed/45)
  if (input.startPressed)
    state.mag = !state.mag
  state.alt = seaLevel - state.y/2 + 2
}

draw = state => {
  clear(11-clamp(Math.floor(state.frame/300),0,3))
  camera(Math.floor(state.x-56), Math.floor((state.y - 60)))
  
  // Sun
  circFill(80  + Math.ceil((128-state.x)/80) + state.x-60, 
           40+state.frame/60 + state.y - 60,
           30, 4)
  
  // Floor
  state.y = drawLand(state) - 8
  
  // Man
  sprite(state.x, state.y, state.animFrame + state.speed, 0, state.flip)
  
  if (state.mag) drawMag(state)
}

// Magnifying HUD

const factor = 3
const magsize = 18

const { sin, cos, tan, pi, sqrt, round, floor } = Math

function drawMag(state) {
  let mag_x = 40 + floor(state.x) - 56
  let mag_y = 120 + floor(state.y) - 60
  let radius = (magsize / 2) * factor
  circFill(mag_x, mag_y, radius + 2, 1)
  circFill(mag_x, mag_y, radius, getPixel(state.x, state.y))
  for (let x of range(-1, magsize * 2)) {
    x = x - floor(magsize / 2)
    for (let y of range(0, magsize)) {
      let hipot = sqrt(((x - 4) * factor) ** 2 + ((y - 4) * factor) ** 2)
      if (hipot < radius - 1) {
        let pix = getPixel(state.x + x, state.y + y)
        rectFill(
          mag_x + (x - 4) * factor - factor / 2,
          mag_y + (y - 4) * factor - factor / 2,
          factor,
          factor,
          pix
        )
      }
    }
  }
  // Borde
  circStroke(mag_x, mag_y, radius + round(factor / 2), 0)
  circStroke(mag_x, mag_y, radius + round(factor / 2) - 1)
  circStroke(mag_x, mag_y, radius + round(factor / 1.2), 1)
  drawHUD(state)
}

function drawHUD(state) {
  printScreen(state, 24, 136, '-RUNNER-')
  printScreen(state, 33, 96, Math.floor(state.alt) + 'M', 0+state.frame%6)
  
}

// Land generation

function f_terrain(x) {
  let base = 68
  if (0 < x && x < 144) return base
  if (x <= 0) x = x + 143
  let freq = 2
  return (
    base -
    108 +
    perlin.octave(x / 1200, 2, 2, 3, 1) * 155 +
    perlin.octave(x / 180, 1, 2, 4.1, 1) * 65 +
    x / 2 -
    base +
    8 +
    Math.sin(3.141595 / 2 - (((x / 512) * freq ** 2) % 3.141595) * 2) * 5
  )
}
/*
  if (x <= 0)
        x = x + 143
    let freq = 4
    return (base - 108 + perlin.octave(x/1200, 2, 2, 3, 1) * 155 - \
        perlin.octave(x/180, 1, 2, 4.1, 1) * 65 + \
        ((x)/2 + sin(pi/2-x/c_width*freq**2 % 3.141595 * 2) * 5))
}*/

function generateLand(state) {
  for (let x of range(Math.floor(state.x - 300), Math.floor(state.x + 300))) {
    state.land[x] = f_terrain(x)
  }
}

function drawOcean(state) {
  line(state.x - 56, seaLevel + 157, state.x + 71, seaLevel+ 157, 4)
  rectFill(state.x - 56, seaLevel + 158, state.x + 71, 64, 5)
}

function drawLand(state) {
  let camPos = Math.floor(state.x) + 43
  let pos = null
  for (let x of range(0, 200)) {
    let col = camPos + 100 - x
    let h = state.land[col]
    if (col == Math.floor(state.x) + 4) {
      pos = Math.floor(h)
    }
    setPixel(col, h, Math.floor(col/10)%2 +1)
    line(col, h + 1, col, 100 + state.y, 3)
  }
  if (state.alt < 64) {
    drawOcean(state)
  }
  return pos
}

// Utilities

function toScreen(state, x, y) {
  return { x: x + floor(state.x) - 56, y: y + floor(state.y) - 60 }
}

function printScreen(state, x, y, text, color) {
  let pos = toScreen(state, x, y)
  print(pos.x, pos.y, text, color)
}

// This is a port of Ken Perlin's Java code. The
// original Java code is at http://cs.nyu.edu/%7Eperlin/noise/.
// Note that in this version, a number from 0 to 1 is returned.
// Taken from http://asserttrue.blogspot.com/2011/12/perlin-noise-in-javascript_31.html
var perlin = new function() {
  this.noise = function(x, y, z) {
    var p = new Array(512)
    var permutation = [
      151,
      160,
      137,
      91,
      90,
      15,
      131,
      13,
      201,
      95,
      96,
      53,
      194,
      233,
      7,
      225,
      140,
      36,
      103,
      30,
      69,
      142,
      8,
      99,
      37,
      240,
      21,
      10,
      23,
      190,
      6,
      148,
      247,
      120,
      234,
      75,
      0,
      26,
      197,
      62,
      94,
      252,
      219,
      203,
      117,
      35,
      11,
      32,
      57,
      177,
      33,
      88,
      237,
      149,
      56,
      87,
      174,
      20,
      125,
      136,
      171,
      168,
      68,
      175,
      74,
      165,
      71,
      134,
      139,
      48,
      27,
      166,
      77,
      146,
      158,
      231,
      83,
      111,
      229,
      122,
      60,
      211,
      133,
      230,
      220,
      105,
      92,
      41,
      55,
      46,
      245,
      40,
      244,
      102,
      143,
      54,
      65,
      25,
      63,
      161,
      1,
      216,
      80,
      73,
      209,
      76,
      132,
      187,
      208,
      89,
      18,
      169,
      200,
      196,
      135,
      130,
      116,
      188,
      159,
      86,
      164,
      100,
      109,
      198,
      173,
      186,
      3,
      64,
      52,
      217,
      226,
      250,
      124,
      123,
      5,
      202,
      38,
      147,
      118,
      126,
      255,
      82,
      85,
      212,
      207,
      206,
      59,
      227,
      47,
      16,
      58,
      17,
      182,
      189,
      28,
      42,
      223,
      183,
      170,
      213,
      119,
      248,
      152,
      2,
      44,
      154,
      163,
      70,
      221,
      153,
      101,
      155,
      167,
      43,
      172,
      9,
      129,
      22,
      39,
      253,
      19,
      98,
      108,
      110,
      79,
      113,
      224,
      232,
      178,
      185,
      112,
      104,
      218,
      246,
      97,
      228,
      251,
      34,
      242,
      193,
      238,
      210,
      144,
      12,
      191,
      179,
      162,
      241,
      81,
      51,
      145,
      235,
      249,
      14,
      239,
      107,
      49,
      192,
      214,
      31,
      181,
      199,
      106,
      157,
      184,
      84,
      204,
      176,
      115,
      121,
      50,
      45,
      127,
      4,
      150,
      254,
      138,
      236,
      205,
      93,
      222,
      114,
      67,
      29,
      24,
      72,
      243,
      141,
      128,
      195,
      78,
      66,
      215,
      61,
      156,
      180
    ]
    for (var i = 0; i < 256; i++) p[256 + i] = p[i] = permutation[i]

    var X = Math.floor(x) & 255, // FIND UNIT CUBE THAT
      Y = Math.floor(y) & 255, // CONTAINS POINT.
      Z = Math.floor(z) & 255
    x -= Math.floor(x) // FIND RELATIVE X,Y,Z
    y -= Math.floor(y) // OF POINT IN CUBE.
    z -= Math.floor(z)
    var u = fade(x), // COMPUTE FADE CURVES
      v = fade(y), // FOR EACH OF X,Y,Z.
      w = fade(z)
    var A = p[X] + Y,
      AA = p[A] + Z,
      AB = p[A + 1] + Z, // HASH COORDINATES OF
      B = p[X + 1] + Y,
      BA = p[B] + Z,
      BB = p[B + 1] + Z // THE 8 CUBE CORNERS,

    return scale(
      lerp(
        w,
        lerp(
          v,
          lerp(
            u,
            grad(p[AA], x, y, z), // AND ADD
            grad(p[BA], x - 1, y, z)
          ), // BLENDED
          lerp(
            u,
            grad(p[AB], x, y - 1, z), // RESULTS
            grad(p[BB], x - 1, y - 1, z)
          )
        ), // FROM  8
        lerp(
          v,
          lerp(
            u,
            grad(p[AA + 1], x, y, z - 1), // CORNERS
            grad(p[BA + 1], x - 1, y, z - 1)
          ), // OF CUBE
          lerp(
            u,
            grad(p[AB + 1], x, y - 1, z - 1),
            grad(p[BB + 1], x - 1, y - 1, z - 1)
          )
        )
      )
    )
  }
  function fade(t) {
    return t * t * t * (t * (t * 6 - 15) + 10)
  }
  function lerp(t, a, b) {
    return a + t * (b - a)
  }
  function grad(hash, x, y, z) {
    var h = hash & 15 // CONVERT LO 4 BITS OF HASH CODE
    var u = h < 8 ? x : y, // INTO 12 GRADIENT DIRECTIONS.
      v = h < 4 ? y : h == 12 || h == 14 ? x : z
    return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v)
  }
  function scale(n) {
    return (1 + n) / 2
  }

  this.octave = function(x, y, z, octaves, persistence) {
    let total = 0
    let frequency = 1
    let amplitude = 1
    let maxValue = 0 // Used for normalizing result to 0.0 - 1.0
    for (let i = 0; i < octaves; i++) {
      total +=
        this.noise(x * frequency, y * frequency, z * frequency) *
        amplitude
      maxValue += amplitude
      amplitude *= persistence
      frequency *= 2
    }
    return total / maxValue
  }
}()
