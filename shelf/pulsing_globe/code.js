// title: Pulsing globe
//
// original by gabrielflorit
// https://script-8.github.io/?id=bab65b680a9fcb5cc1f8325d285ec9d0

function init(state) {
  initialState = {
    o: 0
  }
  Object.assign(state, initialState)
}

update = s => {
 s.o += 8
}

draw = s => {
  clear()
  const st = 15
  const l = 365
  range(st).forEach(r => {
    range(s.o, s.o + l, st).forEach((i, j) => {
      const a = i * Math.PI / 180
      rectFill(
        100 + (78 / st * r) * Math.cos(a),
        80 + 78 * Math.sin(a),
        1, 1, j + r)
    })
  })
}