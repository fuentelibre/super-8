
function init(state) {
  state.t = 0
  state.vel = 0
  state.x = 100
  state.y = 80
}

function update(state, input, elapsed) {
  state.t++
  if (input.selectPressed) {
    state.play = !state.play
  }
  if (input.left) {
    if (state.vel > 0)
    if (state.t % (12 - state.vel) == 0) state.x--
  }
  if (input.right) {
    if (state.vel > 0)
    if (state.t % (12 - state.vel) == 0) state.x++
  }
  if (input.down) {
    state.vel = clamp(state.vel+1, 0, 10)
    state.y = 80 + state.vel * 2
  }
  if (input.up) {
    state.vel = clamp(state.vel-1, 0, 10)
    state.y = 80 + state.vel * 2
  }
}

function draw (state) {
  clear(9)
  rectFill(0, 80, 200, 80, 2)
  line(0, 80, 200, 80, 3)
  if (state.vel > 0) {
    drawRora(state.x - 16, state.y - 24, Math.round(state.t/(12-state.vel))%5)
  }
  else {
    drawRora(state.x - 16, state.y - 24, 0)
  }
}

function drawRora(x, y, frame) {
  for (let row in range(0, 3)) {
    sprite(x + 0, y+row * 8, 16*row + 0)
    sprite(x + 8, y+row * 8, 16*row + 1)
    sprite(x + 16, y+row * 8, 16*row + 2)
    sprite(x + 24, y+row * 8, 16*row + 3)
  }
  offset = {
    0: 16*3,
    1: 4,
    2: 4 + 16*3,
    3: 8,
    4: 8 + 16*3
  }
  for (let row in range(0, 3)) {
    sprite(x + 0, y + 24 + row*8, row*16 + offset[frame] + 0)
    sprite(x + 8, y + 24 + row*8, row*16 + offset[frame] + 1)
    sprite(x + 16, y + 24 + row*8, row*16 + offset[frame] + 2)
    sprite(x + 24, y + 24 + row*8, row*16 + offset[frame] + 3)
  }
}