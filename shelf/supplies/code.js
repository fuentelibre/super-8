// title: Supplies
// original at https://script-8.github.io/?id=3bd3df32f4784924969edc037d3c074c
// by Thykka

init = state => {
  state.time = 0;
  state.copter = { x: 67, y: 14, xSpeed: 0, ySpeed: 0 };
  state.dude = { };
  state.tractor = { x: -22, y: 98, speed: 0.5 };
  state.points = 0;
  state.maxY = 90;
  state.pendingPoints = false;
  state.showHelp = true;
}

update = (state, key, elapsed) => {
  state.time += elapsed / 42;
  state.delta = elapsed * (1 / 24);
  
  if(state.showHelp) {
    if(key.a) state.showHelp = false;
  }
  updateCopter(state, key);
  updateTractor(state);
  updateDude(state, key);
  
  // Warranty void if seal broken
  // if(key.b) state.points += 100;
}

function updateCopter(state, key) {
  if(!key.left && !key.right && !key.up && !key.down) {
    const [xR, yR] = [random() - 0.5, random() - 0.5];
    state.copter.xSpeed += xR / 32 * state.tractor.speed * state.delta;
    state.copter.ySpeed += yR / 64 * state.tractor.speed * state.delta
  }
  if(key.left) state.copter.xSpeed -= 0.2 * state.delta;
  if(key.right) state.copter.xSpeed += 0.2 * state.delta;
  if(key.down) state.copter.ySpeed += 0.1 * state.delta;
  if(key.up) state.copter.ySpeed -= 0.1 * state.delta;
  // decelerate
  if(state.copter.xSpeed !== 0) {
    state.copter.x += state.copter.xSpeed * state.delta;
    state.copter.xSpeed *= .95;
  }
  if(state.copter.ySpeed !== 0) {
    state.copter.y += state.copter.ySpeed * state.delta;
    state.copter.ySpeed *= .95;
  }
  // limit
  if(state.copter.y < 2) {
    state.copter.y = 2;
    state.copter.ySpeed = 0;
  }
  if(state.copter.y > state.maxY) {
    state.copter.y = state.maxY;
    state.copter.ySpeed = 0;
  }
  
  if(state.copter.x < 8) { state.copter.x = 8; }
  if(state.copter.x > 199 - 12) { state.copter.x = 199 - 12; }
}

function updateTractor(state) {
  state.tractor.x += state.tractor.speed * state.delta;
  if(state.tractor.x > 226) state.tractor.x = -20;
}

function updateDude(state, key) {
  if(!state.dude.falling && key.a) {
    state.pendingPoints = getCurrentPoints(state);
    state.dude.x = state.copter.x - 3;
    state.dude.y = state.copter.y + 6;
    state.dude.falling = true;
    state.dude.mode = 0;
  }
  if(state.dude.falling && !state.dude.despawn) {
    state.dude.y+=state.delta
    
    if(
      state.dude.y >= state.tractor.y + 5 &&
      state.dude.y <= state.tractor.y + 8 &&
      state.dude.x > state.tractor.x - 9 &&
      state.dude.x < state.tractor.x + 2
    ) {
      state.dude.despawn = state.time;
      state.dude.y = state.tractor.y + 4;
    }
    
    if(state.dude.y > 135) {
      state.dude.falling = false;
      if(state.points > 0) {
        state.points = state.points * 0.5 | 0;
        state.tractor.speed /= 1.125;
      }
    }
  }
  
  if(state.dude.despawn) {
    const delta = state.time - state.dude.despawn;
    if(delta > 20) {
      state.dude.falling = false;
      state.dude.despawn = false;
      state.points+= state.pendingPoints;
      state.tractor.speed *= 1.125;
      state.pendingPoints = false;
    } else {
      state.dude.x = state.tractor.x - 3;
      if(delta > 5 && state.dude.mode == 0) {
        state.dude.mode = 1;
      }
    }
  }
}

function getCurrentPoints(state) {
  return 2 * state.tractor.speed * (
    102 - (state.copter.y / state.maxY * 102)
  ) | 0;
}
draw = state => {
  clear(6);
  map();
  drawLogo(state);
  drawHelp(state);
  drawScore(state);
  drawHeight(state);
  drawTractor(state.tractor)
  drawDude(state.dude, state.time);
  drawCopter(state.copter, state.time);
}

function drawLogo(state) {
  const time = (state.time - 25);
  const spriteOffset = 5
  const duration = 200;
  const fade = 12;
  let shade = 0;
  if(time <= fade) {
    const lt = (fade - time) / fade;
    shade = Math.floor(0 - lt * 4)
  } else if(time >= duration - fade && time <= duration) {
    const lt = (duration - time) / fade;
    shade = Math.floor(-2 + lt * 3);
  } else if (time >= duration) return;
  [...'supplies'].forEach((l, i, msg) => {
    const x = i * 8 + 70;
    const y = Math.max(55, Math.min(101, 24 - time*2 + (i+4) * 6));
    sprite(x, y, i + spriteOffset, shade)
  })
}

function drawHelp(state) {
  if(!state.showHelp) return;
  const time = state.time - 100;
  const duration = 192;
  const fade = 10;
  let color = 3;
  if(time < 0 || time >= duration) {
    return;
  } else if (time <= fade) {
    const lt = (fade - time) / fade;
    color = 2 + Math.floor(lt * 3);
  } else if(time >= duration - fade && time <= duration) {
    const lt = (duration - time) / fade;
    color = 2 + Math.floor(3 - lt * 3);
  }
  
  print(45, 10, 'Hold (A) to drop, arrows to move', color);
}

function drawScore(state) {
  const win = '!'.repeat(Math.max(0, state.points.toFixed(0).length - 3));
  const score = `score: ${ state.points }${ win }`;
  const pending = (state.dude.despawn ? '+' + state.pendingPoints : '');
  const message = score + pending;
  const messageWidth = message.length * 4;

  let x = state.copter.x - messageWidth / 2 + 4;
  if(x < 1) x = 1;
  if(x > 129 - messageWidth) x = 129 - messageWidth;

  print(x, 122, score,  3);

  if(state.dude.despawn) {
    print(x + score.length * 4, 122, pending, 0);
  }
}

function drawHeight(state) {
  if(state.dude.despawn || state.dude.falling) return;
  const points = getCurrentPoints(state);
  let color = 6
  let alt = 0;
  if(points < 1) {
    color = (state.time % 4 > 2 ? 1 : 3)
  } else {
    alt = state.copter.y;
    color =
      alt < 40 ? 6 : 
      alt < 71 ? 5 : 4;
  }
  const message = '(' + points + ')';
  const x = 128 - message.length * 4;
  const y = state.copter.y + (state.copter.x < 104 ? 0 : -8);
  print(x, y, message, color);
}

function drawCopter(copter, time) {
  const animPhase = time / 3 % 1;
  const shade = (90 - copter.y) / 90 * -3 + 1 | 0;
  sprite(copter.x - 8, copter.y - 8, animPhase < 0.5 ? 0 : 2, shade)
  sprite(copter.x,     copter.y - 8, animPhase < 0.5 ? 1 : 3, shade)
  sprite(copter.x - 8, copter.y, 16, shade)
  sprite(copter.x, copter.y, 17, shade)
  sprite(copter.x + 8, copter.y, animPhase < 0.33 ? 18 : animPhase < 0.67 ? 19 : 20, shade)
}

function drawTractor(tractor, time) {
  const dist = Math.abs(99 - tractor.x) / 100;
  const shade = Math.round(0 - dist * 2);
  sprite(tractor.x - 8, tractor.y,     32, shade);
  sprite(tractor.x,     tractor.y,     33, shade);
  sprite(tractor.x + 8, tractor.y,     34, shade);
  sprite(tractor.x - 8, tractor.y + 8, 48, shade);
  sprite(tractor.x,     tractor.y + 8, 49, shade);
  sprite(tractor.x + 8, tractor.y + 8, 50, shade);
  sprite(tractor.x + 16,tractor.y + 8, 51, shade);
}

function drawDude(dude, time) {
  if(!(dude.falling | dude.despawn)) return;
  const animPhase = time / 4 % 1;
  const spriteIndex = !dude.mode ?
    (animPhase < 0.5 ? 36 : 52) :
    4;
  sprite(dude.x - 4, dude.y - 4, spriteIndex);
}