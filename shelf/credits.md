# Credits

Examples in this directory may come from different sources and are not necessarilly covered by SUPER8's license.

In cases when examples have been derived, a link to the original has been made a comment. Thanks to the original authors!
