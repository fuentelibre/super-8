init = (state) => {
  state.x = 50
  state.y = 50
}

update = (state,input) => {
  if (input.left) state.x++
  if (input.right) state.x--
  if (input.up) state.y++
  if (input.down) state.y--
}

draw = (state) => {
  camera(-state.x, -state.y)
  clear(0)
  map(0,0)
  sprite(96-state.x,76-state.y,65)
}
