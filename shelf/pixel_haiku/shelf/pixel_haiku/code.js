// title: Pixel Haiku
// poem by: Narowe @ 7

init = (state) => {
  state.offset  = -64
  state.randomness = 128
}

update = (state, input, elapsed) => {
  if (input.start) state.randomness = 128
  if (state.randomness > 2) state.offset = (state.offset + 0.2) % 32
  else state.offset = 8 + ((state.offset - 8) * 0.7)
  state.randomness = state.randomness * 0.99
}

draw = state => {
  clear(1)
  range(0, 128).forEach(function(x) {
    let shuffle = random(state.randomness)
    shuffle = Math.floor(0 - shuffle / 2)
    sprite((x * 8) % 128 + 36, Math.floor(x / 16) * 8 + state.offset + 10, x + shuffle, -1)
  })
  if (state.randomness > 2) {
    print(80, 125, '... RECIBIENDO ...', Math.floor(state.randomness))
  } else {
    print(68, 99, 'El mar con olas', 3)
    print(64, 109, 'La naturaleza es', 3)
    print(69, 119, 'Verde de Vida', 3)
    print(75, 129, '10-02-2019', 5)
  }
}
