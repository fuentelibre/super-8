
// Based on a p5.js tutorial by Emily Xie
// * https://www.youtube.com/watch?v=S1TQCi9axzg

function init(state) {
  state.t = 0
  state.streams = range(0, 20).map(createStream)
  console.log(state.symbols)
}

function update(state, input, elapsed) {
  state.t++
  state.streams.map(
    (stream)=> {
      stream.symbols.map(
        (symbol)=> {
            if (state.t % symbol.interval==0) {
              symbol.symbol = validChar()
            }
            symbol.y = symbol.y + stream.v
            if (symbol.y > 160)
              symbol.y = 0
        }
      )
    }
  )
}

function validChar() {
  return String.fromCharCode(0x30A0 + Math.round(random(0, 80)))
}

function draw (state) {
  clear()
  p5.background(0, 150)
  p5.textFont('symbol')
  p5.textSize(10)
  p5.fill(0, 255, 70)
  state.streams.map(
    (stream)=> {
      stream.symbols.map(
        (symbol)=> {
          if (symbol.first) {
            p5.fill(180, 255, 180)
          } else {
            p5.fill(0, 255, 70)
          }
          p5.text(symbol.symbol, stream.x, symbol.y)
        }
      )
    }
  )
}

function createStream(x) {
  let tail = random(6, 16)
  return {
    symbols: range(0, tail).map(createSymbol),
    x: 0 + x * 10,
    v: random(3, 10)
  }
}

function createSymbol(y) {
	return {
      symbol: validChar(),
      y: 0 - y * 10,
      interval: random(2, 20),
      first: y==0 && random(0, 10)>4
	}
}