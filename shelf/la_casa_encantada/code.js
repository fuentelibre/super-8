
init = (state) => {
  state.x = 50
  state.y = 50
}

update = (state,input) => {
  if (input.left) state.x--
  if (input.right) state.x++
  if (input.up) state.y--
  if (input.down) state.y++
}



function draw(state) {
 clear(7)
 map(0,0)
 sprite(state.x, state.y, 9)
}