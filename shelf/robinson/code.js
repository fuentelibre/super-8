// title: Robinson
//

init = (state) => {
  Object.assign(state, {
    hero : { x : 10,
             y : 1200, 
             flip : false },
    plants : [ [ 70, 30 ],
               [ 73, 44 ],
               [ 88, 24 ],
               [ 130, 31 ],
               [ 163, 16 ],
               [ 75, 35 ] ],
    bushes : [ [ 65, 50 ],
               [ 68, 15 ],
               [ 141, 34 ],
               [ 65, 43 ]],
    animTick : 0,
    animStep : 90,
    frame : 16,
    elapsed : 0
  })
}
update = (state, input, elapsed) => {
  state.elapsed += elapsed
  // camera(state.hero.x - 64, state.hero.y - 64)
  var pose = 'stand'
  if ( input.down ) {
    state.hero.y += 1
    pose = 'run'
  }
  else if ( input.up ) {
    state.hero.y -= 1
    pose = 'run'
  }
  if ( input.right ) {
    state.hero.x += 1
    pose = 'run'
    state.hero.flip = false
  }
  else if ( input.left ) {
    state.hero.x -= 1
    pose = 'run'
    state.hero.flip = true
  }
  state.hero.x = clamp(state.hero.x, 0, 192)
  state.hero.y = clamp(state.hero.y, 0, 152)
  
  if (state.elapsed > state.animTick + state.animStep) {
    state.animTick = state.elapsed
    if (pose == 'run') {
      if (state.frame == 0) { state.frame = 1 } else { state.frame = 0 }
    } else if (pose == 'stand') {
      state.frame = 16
    }
  }
}

draw = (state) => {
  clear()
  map(0, 0)
  for (let plant of state.plants) {
    sprite(plant[0], plant[1], 4)
  }
  for (let bush of state.bushes) {
    sprite(bush[0], bush[1], 5)
  }
  sprite(state.hero.x, state.hero.y, state.frame, -7, state.hero.flip)
}