// title: Tetras
// original by MarkGamed7794
// from https://script-8.github.io/?id=b9627a117988bde2742e5e82e5a9fda1

var board = [
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0,0,0,0],
]

var nextArr = shuffle([0,1,2,3,4,5,6]);
var bagArr = shuffle([0,1,2,3,4,5,6]);
var blockX = 3
var blockY = 0
var blockRotation = 0
var currentBlock = 0
var next = 0
var aah = false;
currentBlock = next;
next = nextArr.shift();
if(nextArr.length < 7){nextArr.push(bagArr.shift())}
currentBlock = next;
next = nextArr.shift();
if(nextArr.length < 7){nextArr.push(bagArr.shift())}
var downTimer = 0
var dropTimer = 0
var linesCleared = 0
var gameover = false;
var message = "";
var messageTimer = 0;
var combo = -1;

var score = 0
var lines = 0
var level = 0

var blocks = [
  ["0100010001000100", "0000111100000000", "0010001000100010", "0000000011110000"], // I
  ["0200020022000000", "2000222000000000", "0220020002000000", "0000222000200000"], // J
  ["0100110001000000", "0100111000000000", "0100011001000000", "0000111001000000"], // T
  ["0200020002200000", "0000222020000000", "2200020002000000", "0020222000000000"], // L
  ["0330330000000000", "0300033000300000", "0000033033000000", "3000330003000000"], // S
  ["3300033000000000", "0030033003000000", "0000330003300000", "0300330030000000"], // Z
  ["0000011001100000", "0000011001100000", "0000011001100000", "0000011001100000"]  // O
]

function checkCollision(){
  for(var y = 0;y < 4;y++){
    for(var x = 0;x < 4;x++){
      if(blockY+y < 20){if(board[blockY+y][blockX+x] != 0 && blocks[currentBlock][blockRotation][y*4+x] != 0){return true;}}
      else{if(blocks[currentBlock][blockRotation][y*4+x] != 0){return true;}}
    }
  }
  return false;
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function drop(){
  var hardDropFlag = false;
  blockY++;
  if(checkCollision()){
    blockY--;
    hardDropFlag = true;
    for(var y = 0;y < 4;y++){
      for(var x = 0;x < 4;x++){
        if(blockX+x < 10 && blockX+x >= 0 && blockY+y < 20 && blocks[currentBlock][blockRotation][y*4+x] != 0) {
          board[blockY+y][blockX+x] = blocks[currentBlock][blockRotation][y*4+x]
        }
      }
    }
    blockX = 3;
    blockY = 0;
    currentBlock = next;
    next = nextArr.shift();
    if(nextArr.length < 7){nextArr.push(bagArr.shift())}
    if(bagArr.length == 0){bagArr = shuffle([0,1,2,3,4,5,6]);}
    blockRotation = 0;
    linesCleared = 0
    if(checkCollision()){gameover = true;}
    for(var l = 0;l < board.length;l++){
      if(!board[l].includes(0)){
        board.splice(l,1)
        board.unshift([0,0,0,0,0,0,0,0,0,0])
        lines++;
        linesCleared++;
        level = Math.floor(lines/10)+1
      }
    }
    if(linesCleared > 0){
      combo++;
      score += [40,200,500,1200][linesCleared-1]*level;
      if(combo > 0){
        score += [40,200,500,1200][linesCleared-1]*level*(0.2*combo);
        message = combo + " combo! (x" + (1+(0.2*combo)) + ")"
        messageTimer = 120;
      }
    }else{
      combo = -1;
    }
    return true;
  }
  return false;
}

update = (dum,input) => {
  if(!gameover){
  if(input.rightPressed){
    blockX++;
    if(checkCollision()){blockX--;}
  }
  if(input.leftPressed){
    blockX--;
    if(checkCollision()){blockX++;}
  }
  if(input.down){
    downTimer++;
    if((downTimer > 12 && downTimer%3 == 0) || downTimer == 1){drop();score++;}
  }else{
    downTimer = 0 
  }
  if(input.bPressed || input.upPressed){
    blockRotation = (blockRotation+1)%4;
    if(checkCollision()){blockRotation = (blockRotation+3)%4;}
  }
  if(input.aPressed){
    blockRotation = (blockRotation+3)%4;
    if(checkCollision()){blockRotation = (blockRotation+1)%4;}
  }
  if(input.startPressed){
    while(!drop()){score+=2}
  }
  }
}

draw = () => {
  if(!gameover){
  clear(14)
      
  for(var y = 0;y < board.length;y++){
    for(var x = 0;x < board[0].length;x++){
       rectFill(x*6+20,y*6+20,6,6,board[y][x]*2)
    }
  }
  for(var y = 0;y < 4;y++){
    for(var x = 0;x < 4;x++){
       if(blockX+x < 10 && blockX+x >= 0 && blockY+y < 20 && blocks[currentBlock][blockRotation][y*4+x] != 0) {
         rectFill((blockX+x)*6+20,(blockY+y)*6+20,6,6,blocks[currentBlock][blockRotation][y*4+x]*2)
       }
    }
  }
  
  rectStroke(19,19,board[0].length*6+2, board.length*5+22, 4)
  print(137,11,"NEXT", 0);
  print(136,10,"NEXT", 6);
  for(var y = 0;y < 4;y++){
    for(var x = 0;x < 4;x++){
       if(blocks[next][0][y*4+x] != 0){
         rectFill(136+x*6,24+y*6,6,6,blocks[next][0][y*4+x]*2)
       }
    }
  }
    
  if(messageTimer > 0) {
    print(65,40,message,4);
  }
  
  print(105,71,"SCORE", 0);
  print(104,70,"SCORE", 10);
  print(163-(score.toString().length*4),81,score,0);
  print(162-(score.toString().length*4),80,score,10);
  
  print(105,101,"LINES",0);
  print(104,100,"LINES",11);
  print(163-(lines.toString().length*4),111,lines,0);
  print(162-(lines.toString().length*4),110,lines,11);
  
  print(105,131,"LEVEL",0);
  print(104,130,"LEVEL",2);
  print(163-(level.toString().length*4),141,level,0);
  print(162-(level.toString().length*4),141,level,2);
  
  //I'd put music and SFX here, but they don't work...
  
  dropTimer += 2;
  if(dropTimer > 70 - clamp(level*5,0,65)){drop();dropTimer = 0}
    messageTimer--;
  }else{
    rectFill(22,38,84,42,2);
    rectFill(23,39,82,40,3);
    rectFill(24,40,80,38,2);
    print(46,44,"GAME  OVER",4);
    print(45,43,"GAME  OVER",1);
    
    print(30,56,"SCORE:",4);
    print(29,55,"SCORE:",1);
    print(98-(score.toString().length*4),56,score,4);
    print(97-(score.toString().length*4),55,score,1);
    
    print(30,63,"LINES:",4);
    print(29,62,"LINES:",1);
    print(98-(lines.toString().length*4),63,lines,4);
    print(97-(lines.toString().length*4),62,lines,1);
    
    print(30,70,"LEVEL:",4);
    print(29,69,"LEVEL:",1);
    print(98-(level.toString().length*4),70,level,4);
    print(97-(level.toString().length*4),69,level,1);
    
  }
}