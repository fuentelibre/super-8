
// Jetpac

function init(state) {
  state.man = { x: 50, y: 50, flip: 1, vy: 0, burn: false, carry: null }
  state.pieces = {
   0:[20, 20, 0],
   16:[150, 40, 0],
   32:[92, 128 + 16, 0]
  }
  state.g = 0.2
  state.target = 16
  state.t = 0
  state.sec = 0
  state.jetfuel = 0
  state.current = "pieces"
  state.fuel = null
  state.transparents = [0, 9, 2, 3, 4]
}

function update(state, input, elapsed) {
  state.t += elapsed
  state.sec = Math.round(state.t)
  
  // Generate fuel
  if (state.current=="fuel" &&
      state.fuel==null) {
    if (state.sec%10==random(0,100))
      state.fuel = [random(0,192), -7, 1]
  }
  if (state.fuel!=null) {
    const falling = (state.transparents.indexOf(
      		getPixel((state.fuel[0]+4)%200, state.fuel[1]+11)) != -1)
    if (falling)  {
      state.fuel[1] += state.fuel[2]
      state.fuel[2] += state.g/2
    }
    if (state.man.carry!="F") {
      const detect  = ( ((state.fuel[0]+4)%200 > state.pieces[32][0] + 2 &&
            			 (state.fuel[0]+4)%200 < state.pieces[32][0] + 16 ) &&
      					 (state.fuel[1]+8 > state.pieces[0][1]-3 &&
                          state.fuel[1]+8 < state.pieces[32][1]+8))
      if (detect) {
        state.fuel = null
        state.jetfuel += 20
        state.man.carry = null
        if (state.jetfuel >= 100)
          state.current = "board"
      }
    }
    else {
      state.fuel[2] = 0
    }
  }
  
  // Pieces
  if (state.current!="away") {
    for (const [spr, pos] of Object.entries(state.pieces)) {
      const falling = (getPixel((pos[0]+4)%200, pos[1]+10)==0)
      if (falling && state.man.carry!=spr)
      {
        state.pieces[spr][1] += pos[2]
        state.pieces[spr][2] += state.g
      }
      else {
        state.pieces[spr][2] = 0
      }
    }
  }
  else {
    for (const [spr, pos] of Object.entries(state.pieces)) {
      state.pieces[spr][2] -= state.g * 1.5
      state.pieces[spr][1] += pos[2]
    }
  }
  
  // Collision with Target
  if (state.man.carry == null && state.target in state.pieces) {
    const target = state.pieces[state.target]
    if (state.man.x > target[0] &&
      state.man.x < target[0] + 16) {
    	if (state.man.y > target[1] - 8 &&
            state.man.y < target[1]) {
          		state.man.carry = state.target
        }
    }
  }
  else if (state.man.carry == null && state.fuel != null) {
    const target = state.fuel
    if (state.man.x > target[0]-8 &&
      state.man.x < target[0]+8 ) {
    	if (state.man.y > target[1] - 8 &&
            state.man.y < target[1] + 8) {
          		state.man.carry = state.target
        }
    }

  }

  // Carry
  if (state.man.carry=="F") {
    state.fuel[0] = state.man.x - 4
    state.fuel[1] = state.man.y - 9
  }
  else if (state.man.carry!=null && state.current == "pieces") {
	state.pieces[state.target][0] = state.man.x - 4
    state.pieces[state.target][1] = state.man.y - 9
  }
  else if (state.man.carry!=null && state.current == "fuel") {
    state.fuel[0] = state.man.x - 16 * state.man.flip + 8
    state.fuel[1] = state.man.y - 6
    state.fuel[2] = 0
  }

  // Collision with launchpad
  if (state.man.carry!=null && state.current == "pieces") {
    const piece = state.pieces[state.man.carry]
    if (piece[0] == state.pieces[32][0]) {
      state.man.carry = null
      state.target = (state.target!=0) ? 0: -1
      if (state.target==-1) state.current = "fuel"
    }
  }
  else if (state.man.carry!=null && state.current == "fuel") {
    if ( state.fuel[0] > state.pieces[32][0]+2 &&
        state.fuel[0] < state.pieces[32][0]+6 ) {
      state.man.carry = null
    }
  }
  
  // Man
  state.man.flying = state.transparents.indexOf(
    					getPixel((state.man.x+4)%200, state.man.y+10))!=-1

  if (state.man.flying && state.man.burn) state.man.vx = 2
  else state.man.vx = 1
  
  if (input.right) {
    let detect = (getPixel((state.man.x+8)%200, state.man.y+4)!=0)
    if (!detect) state.man.x += state.man.vx
    state.man.flip = 1
  } 
  else if ( input.left ) {
    let detect = (getPixel((state.man.x-1)%200, state.man.y+4)!=0)
    if (!detect) state.man.x -= state.man.vx
    state.man.flip = 0
  }
  
  state.man.sunken = (getPixel((state.man.x+4)%200, state.man.y+8)!=0)
  state.man.vy += state.g
  
  if (state.man.flying) {
    state.man.sprite = 21
  }
  else  {
    if(state.man.sunken) {
    state.man.vy = -0.22
    }
    else state.man.vy = 0
    state.man.sprite = 36 + Math.round(state.man.x/3) % 2
  }
  
  if (state.man.x < 0) state.man.x = 200 + state.man.x
  if (state.man.x > 200) state.man.x = state.man.x - 200
  
  if (input.up) {
    if (state.man.y > 0)
      state.man.vy = state.man.vy - 0.32  // Afterburner power
    state.man.burn = true
  }
  else {
    state.man.burn = false
  }
  if (state.man.y < 152)
    if (state.man.y > 0 || state.man.vy > 0) state.man.y += state.man.vy
  
  // Board ship
  if (state.current == "board") {
    const detect  = (((state.man.x+5)%200 > state.pieces[32][0] + 2 &&
            		  (state.man.x+2)%200 < state.pieces[32][0] + 14 ) &&
      				  (state.man.y+8 > state.pieces[0][1]-3 &&
                       state.man.y+8 < state.pieces[32][1]+8))
    if (detect) state.current = "away"
  }
}

function draw (state) {
  clear()
  if (state.current=="away") {
    if (state.pieces[0][1] < 70) {
      camera(0, state.pieces[0][1]-70)
  	  print(state.pieces[0][0]-8, state.pieces[0][1], "THE   END", 12)
    }
  }
  else
    camera(0,0)
  
  map(0,0)
    
  // ship
  for (const [spr, pos] of Object.entries(state.pieces)) {
    let color_offset = (state.target == spr)? state.sec % 2 : 0
    if (state.current=='board') color_offset = random(0,1)
    sprite(pos[0], pos[1], spr, color_offset)
    sprite(pos[0]+8, pos[1], +spr+1, color_offset)
    if (spr==32 && state.current=="away") {
      color_offset = random(0,2)
      sprite(pos[0], pos[1]+8+random(1), 48, color_offset)
      sprite(pos[0]+8, pos[1]+8+random(1), 48, color_offset, 1)
    }
  }
  
  // fuel
  if (state.fuel!=null) {
    sprite(state.fuel[0], state.fuel[1], 8, state.sec % 2 - 1)
    print(state.fuel[0]-3, state.fuel[1]-7, "FUEL", 4)
  }
  
  // man
  if (state.current!="away") {
    let man = state.man
    sprite( man.x, man.y, man.sprite, 0, man.flip )
    if (man.burn) {
      let offset = -8 + man.flip * 8 * 2
      let color_offset = -random(0,1)
      sprite( man.x - offset, man.y + 8, 38, color_offset, man.flip )
    }
  }
  
  // labels
  print(88,1,'JET-PAC', 9)
  if (state.current!="away")
  print(166,1,'FUEL '+state.jetfuel, 3)
  
  if (state.current=='pieces')
    print(4,1,'REPAIR SHIP', state.sec % 16 >= 8 ? 0 : 9)
  else if (state.current=='fuel')
    print(4,1,'REFUEL SHIP', 4)
  else if (state.current=='board')
    print(4,1,'BOARD SHIP', state.sec % 16 >= 8 ? 5 : 6)
  
}
