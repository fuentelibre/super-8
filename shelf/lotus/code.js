// title: Lotus

const PI2 = Math.PI * 2;



//\\//\\//\\//\\//\\//\\//\\//\\//
//  Warning! Spaghetti ahead... //
//\\//\\//\\//\\//\\//\\//\\//\\//


init = state => {
  state.frame = 0;
  state.time = 0;
  state.speed = 1000;
  state.spiral = { x: 100, y: 80, rings: 32, r: 90 };
  state.rings = [];
  state.logo = [
    // [0, 16, 32, 48], // L
    // [1, 17, 33, 49], // O
    // [3, 19, 35, 51], // U
    [4, 20, 36, 52], // S
    [7, 23, 39, 55], // C
    [8, 24, 40, 56], // R
    [9, 25, 41, 57], // I
    [10, 26, 42, 58], // P
    [2, 18, 34, 50], // T
    [5, 21, 37, 53], // -
    [6, 22, 38, 54], // 8
  ].map((sprites, index) => ({
    sprites,
    x: 100 - sprites.length * 12 + index * 12,
    y: -8,
    frame: 0,
    shade: -7
  }));
  state.logoSize = 128;
  state.showMessage = false;
}

update = (state, input, elapsed) => {
  state.frame++;
  state.time += elapsed / state.speed;
  updateRings(state);
  updateSpiral(state, input);
  if(input.a) state.showMessage = state.time;

  const lmax = state.logo.length - 1;
  state.logo.forEach((letter, index) => {
    const v = (index + 1) / lmax;
    const osc = Math.cos(index - state.time * PI2) * 0.5 + 0.5;
    const osc2 = Math.sin(index - state.time * PI2 + Math.PI ) * 0.5 + 0.5;
    letter.shade = -7 + osc2 * 10 | 0;
    letter.frame = osc2*osc * (state.logo.length - 1) | 0;
    if(state.time - index/3 > 1 && letter.y < 20) {
      letter.y += 0.67;
    } else if(state.time - index/3 > 10 && letter.y < 128) {
      letter.y += 0.67;
    }
  });
}

function updateSpiral(state, input) {
  if(input.left)  state.speed = Math.min(1e4, state.speed * (1 + 0.01));
  if(input.right) state.speed = Math.max(60, state.speed * (1 - 0.01));
}

function updateRings(state) {
  state.rings = Array.from(
    { length: state.spiral.rings },
    (_, i) => (i + 1) / (state.spiral.rings)
  ).flatMap((phase, index) => {
    const dots = 1 + index | 0;
    return Array.from({ length: dots }, (_, i) => i / dots)
      .map((dot, dotIndex, dotsArray) => {
        const u = dotIndex / 3 * (dot - state.time) / 7 + (phase + state.time / 8) * PI2;
        return [
          (state.spiral.x + Math.sin(state.time/ 4 * PI2 * phase - u) * state.spiral.r * phase),
          (state.spiral.y - Math.cos(state.time/ 4 * PI2 * phase - u) * state.spiral.r * phase),
          (dotIndex - 6 * phase + state.time * 2) % 4 + 3 | 0
        ];
      });
  });
}

draw = state => {
  clear(15);
  drawRings(state.rings);
  if(state.showMessage) {
    drawMessage(state.time - state.showMessage, 5);
  }
  if(state.time < 23)
    drawSprites(state.logo, state.time * 3);
}

function drawMessage(time, maxTime, lineHeight = 11, letterWidth = 4) {
  const t = time / maxTime;
  const h = message.length * lineHeight;
  const td = 0.333;
  const ft = 1 - td;
  let c = 7;
  if(t < td) {
    c = 7 - (t / td) * 5 | 0;
  } else if(t >= ft && t <= 1) {
    const ct = (t - ft) / td;
    c = ct * 7 | 0;
  } else if (t <= 1) {
    c = 0;
  }
  message.forEach((line, index) => {
    if(c >= 7) return;
    const w = line.length * letterWidth;
    const x = 100 - w / 2;
    const y = 80 - h / 2 - t * 4 + index * lineHeight + t * index * 6;
    print(x, y, line, c);
  })
}

function drawRings(rings) {
  rings.filter(ring => {
    return (
      ring[0] >= 0 && ring[0] < 200 &&
      ring[1] >= 0 && ring[1] < 160
    );
  }).sort((a, b) => b[2] - a[2]).forEach(ring => setPixel(...ring));
}

function drawSprites(sprites, animTime) {
  sprites.forEach((s,i) => {
    if(s.x < -7 || s.x > 200 || s.y < -7 || s.y > 133) return;
    sprite(s.x, s.y, s.sprites[s.frame], s.shade);
  });
}

const message = [
  'If you gaze long',
  'enough into an',
  'abyss, the abyss',
  'will gaze back',
  'into you.'
];
