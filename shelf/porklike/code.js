// title: porklike #14

// original by gabrielflorit at 
//   https://script-8.github.io/?id=506850710653d99fc3af566a058683dc

// Watch the real tutorial at https://www.youtube.com/watch?v=HnY7Inp74dw
// TODO:
//   - make a message window when you open the stone tablet

const { floor, sqrt, pow, max } = Math

const createActor = ({ x, y, kind, id, hp }) => ({
  id,
  kind,
  dx: 0,
  dy: 0,
  x,
  y,
  fsmState: 'standing',
  counter: 0,
  hp,
  maxHp: hp,
  targetActorId: null,
  movingTo: { x, y }
})

const createMonsters = () => {
  const monsters = []
  range(25).forEach(x => {
    range(20).forEach(y => {
      const tile = getTile(x, y)
      if (tile && tile.number === 16) {
        monsters.push(
          createActor({
            id: `monster${monsters.length}`,
            kind: 'monster',
            x: x * 8,
            y: y * 8,
            hp: 1
          })
        )
        setTile(x, y, 113)
      }
    })
  })
  return monsters
}

init = state => {
  resetMap()

  state.windows = [
    {
      id: 'hp',
      x: 7,
      y: 90
    }
  ]
  state.fade = 0
  state.mode = 'play'
  state.counter = 0
  state.nextInput = null
  state.actorTurnId = 'player'
  state.floaters = []
  state.actors = [
    createActor({
      id: 'player',
      kind: 'player',
      x: 8,
      y: 8,
      hp: 7
    }),
    ...createMonsters()
  ]
}

const cycles = {
  monster: {
    standing: {
      duration: 16,
      //       steps: [16]
      steps: [16, 17, 16, 18]
    },
    walking: {
      duration: 4,
      //       steps: [17]
      steps: [16, 17, 16, 18]
    }
  },
  player: {
    standing: {
      duration: 24,
      steps: [0, 0, 0, 0, 0, 0, 0, 4]
    },
    walking: {
      duration: 4,
      steps: [1, 2, 3, 0]
    }
  }
}

const env = {
  player: { walkSpeed: 1 },
  monster: { walkSpeed: 2 }
}

const actorMachine = new StateMachine({
  transitions: [
    { name: 'moveEvent', from: 'standing', to: 'walking' },
    { name: 'attackEvent', from: 'standing', to: 'attacking' },
    { name: 'bumpEvent', from: 'standing', to: 'bumping' },
    { name: 'noMoveEvent', from: 'bumping', to: 'standing' },
    { name: 'noMoveEvent', from: 'walking', to: 'standing' },
    { name: 'noMoveEvent', from: 'attacking', to: 'standing' },
    {
      name: 'goto',
      from: '*',
      to: s => s
    }
  ]
})

// Let's think about the combat system.
// Every turn, we first move the player.
// We can move him to a walkable tile.
// Or make him interact with something, e.g. a door or a jar.
// Or attack a monster.
// A player's turn begins with an arrow press,
// and ends when the corresponding state has finished.

// At that point, the monsters' turns begin.
// They will do the same thing: move to a walkable tile,
// as decided by whatever pathfinding we choose.
// They cannot interact with objects, but rather
// they will bump into them, e.g. no opening doors.
// They do not attack each other.
// But they can attack the player.
// When all the monsters have finished their
// corresponding state, then we can take our turn.

const moveActors = (state, input) => {
  // Get the input buffer - we'll update it.
  let { nextInput } = state

  // Get the inputs we're interested in.
  // If nextInput is not null, use that.
  const arrowIsPressed = arrowPressed(nextInput || input)
  const singleArrow = getSingleArrow(nextInput || input)

  // For every actor,
  state.actors.every((actor, i, array) => {
    // Get the player, for convenience later.
    const player = array.find(actor => actor.kind === 'player')

    // If the player is out of health, break loop.
    if (player.hp <= 0) {
      return
    }

    // Get the properties we'll be updating.
    let {
      x,
      y,
      dx,
      dy,
      fsmState,
      counter,
      sprite,
      flipH,
      kind,
      targetActorId,
      movingTo
    } = actor

    // Check if this actor is the player. We'll use this later.
    const isPlayer = kind === 'player'

    // Reset fsmState.
    actorMachine.goto(fsmState)

    if (actorMachine.state === 'standing') {
      // -------------------------------------------------------------------------
      // ------- STANDING --------------------------------------------------------
      // -------------------------------------------------------------------------

      // Set sprite.
      const { steps, duration } = cycles[kind][fsmState]
      sprite = steps[floor(counter / duration) % steps.length]

      // Set counter.
      counter++

      // Reset speed.
      dx = 0
      dy = 0

      // If it's not our turn,
      if (state.actorTurnId !== actor.id) {
        // if we're the player and there's input,
        // set current input as next input.
        if (isPlayer && arrowPressed(input)) nextInput = input
      } else {
        // If it is our turn,
        // and we're the monster,
        // or if we're the player and there is input,
        if (!isPlayer || arrowIsPressed) {
          // if we're the monsters,
          if (!isPlayer) {
            // set our direction towards the player.
            const direction = getDirectionToActor(x, y, player)
            singleArrow.x = direction[0]
            singleArrow.y = direction[1]
          } else {
            // If we're the player, and nextInput exists,
            // we're consuming it here. Set it to null.
            if (nextInput) nextInput = null
          }

          // Get the target coordinates (where we want to move).
          const targetCoords = [x + singleArrow.x * 8, y + singleArrow.y * 8]

          // Make them map coordinates.
          const targetCoordsMap = targetCoords.map(d => d / 8)

          // Get the target tile.
          const targetTile = getTile(...targetCoordsMap)

          // Get the target actor by trying to find an actor that is moving
          // towards our target tile.
          const targetActor = state.actors.find(
            actor =>
              actor.movingTo.x === targetCoords[0] &&
              actor.movingTo.y === targetCoords[1]
          )

          let nextState

          // Set movingTo to our current location, by default.
          movingTo = { x, y }

          // If the tile is walkable,
          if (targetTile.type === 0) {
            // if there is no actor there,
            if (!targetActor && actorMachine.can('moveEvent')) {
              nextState = 'walk'
              // set movingTo to that tile.
              movingTo = { x: targetCoords[0], y: targetCoords[1] }
            } else if (
              // If there is an actor and it's not our kind,
              targetActor &&
              targetActor.kind !== kind &&
              actorMachine.can('attackEvent')
            ) {
              nextState = 'attack'
            } else if (
              // if there is an actor and it is our kind,
              targetActor &&
              targetActor.kind === kind &&
              actorMachine.can('bumpEvent')
            ) {
              nextState = 'bump'
            }
          } else if (
            // We know the tile is not walkable.
            // If this actor can fire a bump event, do so.
            actorMachine.can('bumpEvent')
          ) {
            nextState = 'bump'
          }

          if (nextState === 'walk') {
            // Set actor dx/dy.
            dx = singleArrow.x * env[kind].walkSpeed
            dy = singleArrow.y * env[kind].walkSpeed

            // Reset the counter.
            counter = 0

            // Finally, move to the next state.
            actorMachine.moveEvent()
          } else if (nextState === 'bump') {
            // If we're the player, interact.
            if (isPlayer) {
              if (targetTile.type === 1) playPhrase(0)
              else if (targetTile.type === 2) {
                for (const [from, to, phrase] of [
                  // door
                  [125, 113, 1],
                  // big box
                  [124, 123, 2],
                  // small box
                  [122, 121, 2],
                  // big vase
                  [120, 113, 5],
                  // small vase
                  [119, 113, 5],
                  // stone tablet,
                  [118, 118],
                  // upstairs
                  [126, 126, 8]
                ]) {
                  if (targetTile.number === from) {
                    setTile(...targetCoordsMap, to)
                    phrase !== undefined && playPhrase(phrase)
                  }
                  // If appropriate, show message.
                  if (from === 118) {
                    state.window = []
                  }
                }
              }
            }

            // Set the initial bump speed.
            dx = singleArrow.x * 0.5
            dy = singleArrow.y * 0.5

            // Next reset the counter.
            counter = 0

            // Finally, move to the next state.
            actorMachine.bumpEvent()
          } else if (nextState === 'attack') {
            // set the initial bump speed,
            dx = singleArrow.x * 0.5
            dy = singleArrow.y * 0.5

            // reset the counter,
            counter = 0

            // set the targetActor id on this actor,
            targetActorId = targetActor.id

            // and move to the next state.
            actorMachine.attackEvent()
          }
        }
      }
    } else if (fsmState === 'walking') {
      // -------------------------------------------------------------------------
      // ------- WALKING ---------------------------------------------------------
      // -------------------------------------------------------------------------
      if (isPlayer) {
        // If pressed, set current input as next input.
        if (arrowPressed(input)) nextInput = input

        // Play the walk phrase on the first frame.
        if (counter === 0) playPhrase(4)
      }

      // Update position.
      x += dx
      y += dy

      // Flip direction.
      flipH = dx ? dx < 0 : flipH

      // Set sprite.
      const { steps, duration } = cycles[kind][fsmState]
      sprite = steps[floor(counter / duration) % steps.length]

      if (counter === 0) {
        // set the actor turn to the next actor in this array,
        state.actorTurnId = array[(i + 1) % array.length].id
      }

      // Update counter.
      counter++

      // If we reached the next tile,
      if (x % 8 === 0 && y % 8 === 0 && actorMachine.can('noMoveEvent')) {
        // reset the counter,
        counter = 0

        // and go to next state.
        actorMachine.noMoveEvent()
      }
    } else if (fsmState === 'bumping') {
      // -------------------------------------------------------------------------
      // ------- BUMPING ---------------------------------------------------------
      // -------------------------------------------------------------------------
      // If pressed, set current input as next input.
      if (isPlayer && arrowPressed(input)) nextInput = input

      // Set the flip direction (only if we're on the first step),
      flipH = !counter && dx ? dx < 0 : flipH

      // move the actor,
      x += dx
      y += dy

      // and if the counter is past a certain point,
      if (counter === 3) {
        // reset the movement direction.
        dx *= -1
        dy *= -1
      }

      if (counter === 0) {
        // set the actor turn to the next actor in this array,
        state.actorTurnId = array[(i + 1) % array.length].id
      }

      // Last, increment the counter.
      counter++

      // If we reached the next tile,
      if (x % 8 === 0 && y % 8 === 0 && actorMachine.can('noMoveEvent')) {
        // reset the counter,
        counter = 0

        // and go to next state.
        actorMachine.noMoveEvent()
      }
    } else if (fsmState === 'attacking') {
      // -------------------------------------------------------------------------
      // ------- ATTACKING -------------------------------------------------------
      // -------------------------------------------------------------------------
      // If pressed, set current input as next input.
      if (isPlayer && arrowPressed(input)) nextInput = input

      // Find the actor we're attacking and its index in the actors array.
      const targetActorIndex = array.findIndex(
        actor => actor.id === targetActorId
      )
      const targetActor = array[targetActorIndex]

      // Make the target flash.
      targetActor.brighten = -Math.floor(counter / 2)

      // If we're on the first attack frame,
      if (counter === 0) {
        // decrement target's hp,
        targetActor.hp--

        // and add the hp hit to floaters.
        state.floaters.push({
          x: targetActor.x,
          y: targetActor.y - 4,
          targetY: targetActor.y - 8,
          counter: 0,
          text: '-1'
        })

        // Play attack sounds:
        if (isPlayer)
          // player attacking
          playPhrase(6)
        // monster attacking
        else playPhrase(7)
      }

      // If the player is bumping into something,
      // set the flip direction (only if we're on the first step),
      flipH = !counter && dx ? dx < 0 : flipH

      // move the player,
      x += dx
      y += dy

      // and if the counter is past a certain point,
      if (counter === 3) {
        // reset the movement direction.
        dx *= -1
        dy *= -1
      }

      // Last, increment the counter.
      counter++

      // If we reached the next tile,
      if (x % 8 === 0 && y % 8 === 0 && actorMachine.can('noMoveEvent')) {
        // reset the target color,
        targetActor.brighten = 0

        // if we are the player,
        // and the target actor is out of health,
        // remove it from array,
        if (isPlayer && !targetActor.hp) {
          array.splice(targetActorIndex, 1)
        }

        // reset the counter,
        counter = 0

        // reset the targetActorId,
        targetActorId = null

        // set the actor turn to the next actor in this array.
        state.actorTurnId = array[(i + 1) % array.length].id

        // and go to next state.
        actorMachine.noMoveEvent()
      }
    }

    actor.x = x
    actor.y = y
    actor.dx = dx
    actor.dy = dy
    actor.fsmState = actorMachine.state
    actor.counter = counter
    actor.sprite = sprite
    actor.flipH = flipH
    actor.targetActorId = targetActorId
    actor.brighten = -10
    actor.movingTo = { x: movingTo.x, y: movingTo.y }

    return true
  })
  state.nextInput = JSON.parse(JSON.stringify(nextInput))
}

// General utility functions

// Gets the direction from x/y to the actor.
// Returns an array.
// For example, if the actor is to the right of the x/y,
// returns [1, 0].
const getDirectionToActor = (x, y, actor) => {
  let result = [0, 0]
  let minDist = 999
  // For the four directions (up, down, right, left),
  for (const direction of [[0, -1], [0, 1], [1, 0], [-1, 0]]) {
    // get the new movement's coordinates,
    const x1 = x + direction[0] * 8
    const y1 = y + direction[1] * 8
    // get the tile on these coordinates,
    const targetTile = getTile(x1 / 8, y1 / 8)
    // and if the tile is walkable,
    if (targetTile.type === 0) {
      // get the distance from the coordinates to the player,
      const thisDist = dist(x1, y1, actor.x, actor.y)
      // and if this distance is smaller than the current minDistance,
      if (thisDist < minDist) {
        // update the minDistance,
        minDist = thisDist
        // and update final direction.
        result = direction
      }
    }
  }
  return result
}

// Euclidean distance.
// Returns a number.
const dist = (x1, y1, x2, y2) => sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2))

// Was an arrow key/button pressed?
// Returns boolean.
const arrowPressed = input =>
  input.leftPressed ||
  input.rightPressed ||
  input.upPressed ||
  input.downPressed

// If an arrow was pressed, returns one in an object format.
// For example:
// If right was pressed, returns { x: 1, y:  0 }.
// If up was pressed,    returns { x: 0, y: -1 }.
const getSingleArrow = input => {
  let array = [0, 0, 0, 0]
  array[
    [
      input.upPressed,
      input.rightPressed,
      input.downPressed,
      input.leftPressed
    ].indexOf(true)
  ] = 1
  return { x: array[1] - array[3], y: -array[0] + array[2] }
}

// Update utility functions

const moveYToTargetY = object => {
  object.y += (object.targetY - object.y) / 10
}

// Moves each floating text up, and removes it after a while.
const updateFloaters = state => {
  state.floaters.forEach((floater, i) => {
    moveYToTargetY(floater)
    floater.counter++
    if (floater.counter > 30) state.floaters.splice(i, 1)
  })
}

// Move each window towards its target.
const updateWindows = state => {
  const player = state.actors.find(actor => actor.kind === 'player')

  state.windows.forEach(window => {
    if (window.id === 'hp') {
      window.lines = [`hp: ${player.hp}/${player.maxHp}`]

      // Only change the HP's targetY if the player
      // is on the left side.
      if (player.movingTo.x < 64 - 16) {
        window.targetY = player.movingTo.y < 64 ? 140 : 7
      }
    }
    moveYToTargetY(window)
  })
}

// Fade screen by the given number. Uses colorSwap.
// Note that this doesn't actually fade anything. It just sets the colors.
// For example, if we supply 1, this will call:
// colorSwap(0, 1)
// colorSwap(1, 2)
// colorSwap(2, 3)
// and etc.
const fade = n => {
  range(7).forEach(i => {
    colorSwap(i, clamp(i + n, 0, 7))
  })
}

// Draw utility functions

// Draw all the floaters texts.
const drawFloaters = state => {
  state.floaters.forEach(floater => {
    outlinePrint(floater.x, floater.y, floater.text, 1, 7)
  })
}

// Draw a window.
const drawWindow = (lines, x, y) => {
  // Find the width of the longest line,
  const width = max(...lines.map(line => line.length)) * 4 + 7
  // calculate the height of all the lines together,
  const height = lines.length * 8 + 5
  // draw a filled rectangle,
  rectFill(x, y, width, height, 7)
  // a stroked rectangle outside,
  rectStroke(x + 1, y + 1, width - 2, height - 2, 4)
  // and finally, print each line.
  lines.forEach((line, i) => {
    print(x + 4, y + 4 + i * 8, line, 2)
  })
}

// Draw the state's windows.
const drawWindows = state => {
  state.windows.forEach(({ x, y, lines }) => {
    drawWindow(lines, x, y)
  })
}

// Prints text with optional outline around it.
const outlinePrint = (x, y, text, color, outline) => {
  range(9).forEach(i => {
    print(x + -1 + (i % 3), y + -1 + i / 3, text, outline)
  })
  print(x, y, text, color)
}

// Game loop

update = (state, input) => {
  // This entire thing is so horrendous. What a hacky state machine
  // to manage game UI. We need something better.
  const player = state.actors.find(actor => actor.kind === 'player')
  if (state.mode === 'play') {
    moveActors(state, input)
    updateFloaters(state)
    updateWindows(state)
    if (player.hp <= 0) {
      state.mode = 'play-to-black'
      state.counter = 0
    } else {
      state.counter++
    }
  } else if (state.mode === 'play-to-black') {
    state.counter++
    if (state.counter > 6) {
      state.fade++
      state.counter = 0
    }
    if (state.fade > 6) {
      state.mode = 'black-to-death'
      state.counter = 0
    }
  } else if (state.mode === 'black-to-death') {
    state.counter++
    if (state.counter > 1) {
      state.fade--
      state.counter = 0
    }
    if (state.fade === 0) {
      state.mode = 'death'
    }
  } else if (state.mode === 'death') {
    // If we pressed an arrow,
    // e.g. because we want to start over,
    if (arrowPressed(input)) {
      // reset.
      state.mode = 'death-to-black'
      state.counter = 0
    }
  } else if (state.mode === 'death-to-black') {
    state.counter++
    if (state.counter > 1) {
      state.fade++
      state.counter = 0
    }
    if (state.fade > 6) {
      init(state)
      state.mode = 'black-to-play'
      state.counter = 0
      state.fade = 7
    }
  } else if (state.mode === 'black-to-play') {
    state.counter++
    if (state.counter > 1) {
      state.fade--
      state.counter = 0
    }
    if (state.fade === 0) {
      state.mode = 'play'
    }
  }
}

draw = state => {
  if (state.mode === 'play') {
    fade(state.fade)
    clear()
    map()
    drawActors(state)
    drawFloaters(state)
    drawWindows(state)
  } else if (state.mode === 'play-to-black') {
    fade(state.fade)
    clear()
    map()
    drawActors(state)
    drawFloaters(state)
  } else if (state.mode === 'black-to-death') {
    fade(state.fade)
    clear()
    outlinePrint(48, 56, 'you died.', 0, 7)
  } else if (state.mode === 'death') {
    fade(state.fade)
    clear()
    outlinePrint(48, 56, 'you died.', 0, 7)
  } else if (state.mode === 'death-to-black') {
    fade(state.fade)
    clear()
    outlinePrint(48, 56, 'you died.', 0, 7)
  } else if (state.mode === 'black-to-play') {
    fade(state.fade)
    clear()
    map()
    drawActors(state)
    drawFloaters(state)
  }

  const debug = 0
  if (debug) {
    const actorInTurn = state.actors.find(
      actor => actor.id === state.actorTurnId
    )
    const player = state.actors.find(actor => actor.kind === 'player')
    print(0, 0, `${player.hp}/${player.maxHp}`, 0)
    //     print(0, 0, `turn: ${actorInTurn.id}`, 0)
    //     print(0, 8, `fsm: ${actorInTurn.fsmState}`, 0)
  }
}

function playPhrase() {}