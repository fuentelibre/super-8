// title: Lunar Lander 1

function init(state) {
  state.x = 36
  state.y = 20
  state.vy = 0
  state.g = 0.16
  state.trust = 0
  state.floor_y = 110
  state.landed = false
  state.xplotion = 50
  state.t = 0
  state.stars = []
}

function drawShip(x, y) {
  offset = -13
  sprite(x, y, 0, offset)
  sprite(x, y + 8, 16, offset)
  sprite(x, y + 16, 32, offset)
  sprite(x + 8, y, 0, 1 + offset, 1)
  sprite(x + 8, y + 8, 16, 1 + offset, 1)
  sprite(x + 8, y + 16, 32, 1 + offset, 1)
}

function drawStars(state) {
  for (let star of state.stars) {
    setPixel(star[0], star[1], 3 + state.t % 3)
  }
}

draw = function(state) {
  clear(8)
  drawStars(state)
  line(0, state.floor_y, 200, state.floor_y, 9)
  rectFill(0, state.floor_y + 1, 
           200, 160-state.floor_y, 13)
  circStroke(140,130,4,14)
  circStroke(150,125,3,14)
  circStroke(30,130,3,14)
  rectStroke(0, 0, 200, 160, 14)
  polyStroke([[33, 110], [33 + 21, 110],
              [33 + 18, 111], [36, 111]], 10)

  if (!state.landed || (state.landed && Math.abs(state.vy) < 1.5)) {
    drawShip(state.x, state.y)
    if (state.landed && Math.abs(state.vy) < 1.5) {
      sprite(56, 102, 1, -4)
    }
  } else {
    for (let i of range(state.xplotion)) {
        circFill(
        state.x + 8 + i / 2 - random(i),
        state.y + 15 - random(i),
        random(i / 5),
        random(3) + 1
      )
    }
    state.xplotion -= 0.2
  }
  if (state.trust) {
    sprite(state.x, state.y + 18, 33, (state.t % 4) - 4)
    sprite(state.x + 7, state.y + 18, 33, (state.t % 4) - 3)
  }
  print(130, 40, 'Alt: ' + state.alt, 5)
  let color = Math.abs(state.vy) < 1 ? 5 : 1
  if (Math.abs(state.vy) > 1.5) color = Math.floor(state.t / 10) % 2 + 1
  print(130, 50, 'Vel: ' + (0 - state.vy.toFixed(2)), color)
  sprite(128, 13, 17, -4)
  sprite(136, 14, 18, -4)
  print(144, 20, "1", state.t % 2 + 4)
}

function makeStars(state) {
  for (let i in range(0, 20)) {
    let x = random(0, 200)
    let y = random(0, 128)
    state.stars.push([x, y])
  }
}

function moveStars(state) {
  for (let star of state.stars) {
    star[0] = (star[0] + 0.01) % 200
    star[1] = (star[1] - 0.01 + 128) % 128
  }
}

// x = x + vt
// v = v + at

update = function(state, input, elapsed) {
  state.t += 1
  if (state.t > 2) {
    let sensor = state.alt < 0
    if (sensor || state.landed) {
      state.landed = true
      state.trust = 0
      state.alt = 'TOUCH'
    }
    moveStars(state)
  }
  else {
    makeStars(state)
  }
  if (state.landed === false) {
    if (input.up) {
      state.trust = 1
    }
    else
      state.trust = 0
    state.alt = (state.floor_y - state.y - 17).toFixed(2) + 1
    let a = - state.g + state.trust
    state.y = state.y + state.vy * elapsed / 200
    state.vy = state.vy - a * elapsed / 200
  }
}