// title: Fire effect
// adapted from http://fabiensanglard.net/doom_fire_psx/

var width = 200
var height = 60
var offset = 100

init = (state)=> 
{
  var fb = []

  range(0, width).forEach((x)=>
  {
    fb[x] = []
    range(0, height-1).forEach((y)=>
    {
      fb[x][y]=7
    })
    fb[x][height-1] = 1
  })
  
  state.firePixels= fb
  state.frame= 0
  state.wind= 0
  state.intensity= 2
}

update = (state, input, ellapsed) => {
  if (input.right) {
    state.wind = clamp(state.wind + 0.3, -2, 2)
  } else if (input.left) {
    state.wind = clamp(state.wind - 0.3, -2, 2)
  } else {
    state.wind = state.wind * 0.8
  }
  if (input.up) {
    state.intensity = clamp(state.intensity + 0.5, 2, 20)
  } else if (input.down) {
    state.intensity = 0
  } else {
    state.intensity = 1
  }
  for (let y of range(height-2, 0, -1)) {
    for (let x of range(0, width)) {
      var rand = Math.round(Math.random());
      var rand2 = Math.round(Math.random());
      var dst = Math.round(clamp(x - rand + rand2 + state.wind, 0, width-1))
      state.firePixels[dst][y-rand * state.intensity] = clamp(state.firePixels[x][y+1] + rand, 1, 8)
    }
  }
}

draw = (state)=> {
  clear(8)
  for (let x of range(0, width)) {
    for (let y of range(0, height)) {
      line(x,y + offset,x,y + offset,state.firePixels[x][y])
    }
  }
  print(10,10, this.p5.frameRate(), 9)
}