
function init(state) {
  state.angle = 90
  state.va = 0
  state.vel = { x: 0,
                y: 0 }
  state.pos = { x: 96,
                y: 74 }
  state.cam = { x: 0,
                y: 0 }
  state.prevCam = null
}

function update(state, input, elapsed) {
  state.angle += state.va
  var radians = state.angle / 360 * Math.PI * 2
  
  if (input.left) {
    state.va += 0.2
  } else if (input.right) {
    state.va -= 0.2
  }
  if (input.up) {
    state.vel.x = state.vel.x + Math.cos(radians)
    state.vel.y = state.vel.y - Math.sin(radians)
  }

  if (state.angle < 0) {
    state.angle = 360 + state.angle
  } else if (state.angle > 360) {
    state.angle = state.angle - 360
  }
  
  state.pos.x += state.vel.x / 100
  state.pos.y += state.vel.y / 100
  
  state.prevCam = state.cam
  if (state.pos.x - state.cam.x > 160) state.cam.x += state.vel.x / 100
  else if (state.pos.x - state.cam.x < 40) state.cam.x += state.vel.x / 100
  
  if (state.pos.y - state.cam.y > 120) state.cam.y += state.vel.y / 100
  else if (state.pos.y - state.cam.y < 40) state.cam.y += state.vel.y / 100
}

function draw (state) {
  var color = 13
  
  // Determinar cual Sprite se usa
  var cuadrante = Math.floor(state.angle / 90)
  var angulo = state.angle % 90
  
  var nave_spr, 
      flip_h=false,
      flip_v=false
  
  if (cuadrante==2) {
    flip_h=true
    flip_v=true
  } else if (cuadrante==1) {
    flip_h=true
    angulo=90 - angulo
  } else if (cuadrante==3) {
    flip_v=true
    angulo=90 - angulo
  }
  
  if (0 <= angulo && angulo < 18) nave_spr = 3
  else if (18 <= angulo && angulo < 36) nave_spr = 2
  else if (36 <= angulo && angulo < 54) nave_spr = 17
  else if (54 <= angulo && angulo < 72) nave_spr = 1
  else if (72 <= angulo && angulo <= 90) nave_spr = 0
 
  camera(state.cam.x, state.cam.y)
  if (state.prevCam.x != state.cam.x) {
	state.vel.x = 0
  }
  clear(color)
  drawGrid(state)
  sprite(state.pos.x, state.pos.y, nave_spr, 0, flip_h, flip_v)
  drawHUD(state)
}

function drawGrid(state) {
  const gridColor = 5,
        x = Math.floor(state.cam.x / 10) * 10,
  		y = Math.floor(state.cam.y / 10) * 10,
        gridSize = 10

  for (i = x-1; i < state.cam.x+200; i += gridSize) {
  	line(i, 0+state.cam.y, i, 160+state.cam.y, gridColor)
  }
  for (i = y-1; i < state.cam.y+160; i += gridSize) {
  	line(0+state.cam.x, i, 200+state.cam.x, i, gridColor)
  }
  
  //range(4).map( (x)=> {
  //  line((x+1)*40+state.cam.x, 0+state.cam.y, (x+1)*40+state.cam.x, 160+state.cam.y, gridColor)
  //})
  //range(3).map( (y)=> {
  //  line(0+state.cam.x, (y+1)*40+state.cam.y, 
  //       200+state.cam.x, (y+1)*40+state.cam.y, gridColor)
  //})
}

function drawHUD(state) {
  const x = state.cam.x,
        y = state.cam.y,
        color = 3
  rectFill(x, y, 200, 16, 14)
  rectFill(x, y+151, 200, 16, 14)
  print(9+x, 1+y, "Ang " + Math.floor(state.angle), color)
  print(9+x, 9+y, "VA  " + Math.floor(state.va*10), color)
  print(160+x, 1+y, "Vel " + Math.abs(Math.floor(
    				state.vel.x * state.vel.y*100))/100, color)
  print(70+x, 153+y, "POS (" + Math.round(state.pos.x) + ", " + 
        				  Math.round(state.pos.y) + ")", color)
}