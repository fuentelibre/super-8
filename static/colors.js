import clamp from 'lodash/clamp'
import { shouldHighlight } from './frameBufferCanvasAPI/index'

// Lookup table for triplet arrays containing R, G, and B values.
const triplets = [
                [26, 28, 44],
                [93, 39, 93],
                [177, 62, 83],
                [239, 125, 87],
                [255, 205, 117],
                [167, 240, 112],
                [56, 183, 100],
                [37, 113, 121],
                [41, 54, 111],
                [59, 93, 201],
                [65, 166, 246],
                [115, 239, 247],
                [244, 244, 244],
                [148, 176, 194],
                [86, 108, 134],
                [51, 60, 87]
                ]

/* Original Script8 Palette
  [246, 214, 189],
  [195, 163, 138],
  [153, 117, 119],
  [129, 98, 113],
  [78, 73, 95],
  [32, 57, 79],
  [15, 42, 63],
  [8, 20, 30]
*/

/* Dawnbringer 32
                    [0, 0, 0],
                    [34, 32, 52],
                    [69, 40, 60],
                    [102, 57, 49],
                    [143, 86, 59],
                    [223, 113, 38],
                    [217, 160, 102],
                    [238, 195, 154],
                    [251, 242, 54],
                    [153, 229, 80],
                    [106, 190, 48],
                    [55, 148, 110],
                    [75, 105, 47],
                    [82, 75, 36],
                    [50, 60, 57],
                    [63, 63, 116],
                    [48, 96, 130],
                    [91, 110, 225],
                    [99, 155, 255],
                    [95, 205, 228],
                    [203, 219, 252],
                    [255, 255, 255],
                    [155, 173, 183],
                    [132, 126, 135],
                    [105, 106, 106],
                    [89, 86, 82],
                    [118, 66, 138],
                    [172, 50, 50],
                    [217, 87, 99],
                    [215, 123, 186],
                    [143, 151, 74],
                    [138, 111, 48]
*/

/* Resurrect 64
                        [46, 34, 47],
                        [62, 53, 70],
                        [98, 85, 101],
                        [150, 108, 108],
                        [171, 148, 122],
                        [105, 79, 98],
                        [127, 112, 138],
                        [155, 171, 178],
                        [199, 220, 208],
                        [255, 255, 255],
                        [110, 39, 39],
                        [179, 56, 49],
                        [234, 79, 54],
                        [245, 125, 74],
                        [174, 35, 52],
                        [232, 59, 59],
                        [251, 107, 29],
                        [247, 150, 23],
                        [249, 194, 43],
                        [122, 48, 69],
                        [158, 69, 57],
                        [205, 104, 61],
                        [230, 144, 78],
                        [251, 185, 84],
                        [76, 62, 36],
                        [103, 102, 51],
                        [162, 169, 71],
                        [213, 224, 75],
                        [251, 255, 134],
                        [22, 90, 76],
                        [35, 144, 99],
                        [30, 188, 115],
                        [145, 219, 105],
                        [205, 223, 108],
                        [49, 54, 56],
                        [55, 78, 74],
                        [84, 126, 100],
                        [146, 169, 132],
                        [178, 186, 144],
                        [11, 94, 101],
                        [11, 138, 143],
                        [14, 175, 155],
                        [48, 225, 185],
                        [143, 248, 226],
                        [50, 51, 83],
                        [72, 74, 119],
                        [77, 101, 180],
                        [77, 155, 230],
                        [143, 211, 255],
                        [69, 41, 63],
                        [107, 62, 117],
                        [144, 94, 169],
                        [168, 132, 243],
                        [234, 173, 237],
                        [117, 60, 84],
                        [162, 75, 111],
                        [207, 101, 127],
                        [237, 128, 153],
                        [131, 28, 93],
                        [195, 36, 84],
                        [240, 79, 120],
                        [246, 129, 129],
                        [252, 167, 144],
                        [253, 203, 176]
*/

// Lookup table for the css color strings.
const rgbStrings = triplets.map(([r, g, b]) => `rgb(${r},${g},${b})`)

// Lookup tree for the combined integer representation of each color.
let intLookup = []

// Loops over each of the color indexes, looks up associated color triplet, and
// calculates the equivalent integer representation by bit shifting each byte
// into the correct position.
for (let i = 0; i < triplets.length; i++) {
  let values = triplets[i]

  // Shift each byte into the correct integer position.
  let possiblyNegativeInteger =
    (255 << 24) | // Alpha byte
    (values[2] << 16) | // B byte
    (values[1] << 8) | // G byte
    values[0] // R byte

  // Set the positive version of the above calculated integer into the lookup
  // table. A bitwise right shift of 0 places forces the number to be
  // interpreted as a positive integer.
  intLookup[i] = possiblyNegativeInteger >>> 0

  // For example, intLookup[0] = 4290631414.
}

// The opposite of the intLookup. Each index is the color integer, and the value
// is the SCRIPT-8 integer color.
const reverseIntLookup = {}
for (let i = 0; i < triplets.length; i++) {
  reverseIntLookup[intLookup[i]] = i
}

const colors = {
  rgb(i) {
    i = parseInt(i, 36)
    return rgbStrings[i % rgbStrings.length]
  },

  triplet(i) {
    return triplets[i % triplets.length]
  },

  int(i) {
    let index = i % intLookup.length;
    // `shouldHighlight` is injected in the use code when the current call is under the mouse. If it is true, all colors
    // drawn are brightened by 2 steps if they can be, or darkened by two steps if they are already too bright
    if (shouldHighlight) {
      if (index > 2) index = clamp(+index - 2, 0, intLookup.length)
      else index += 2
    }
    return intLookup[index]
  },

  // Looks up the integer value in the reverseIntLookup table. If it doesn't
  // exist, then the background color of 7 is returned instead.
  lookupInt(int) {
    if (int in reverseIntLookup) {
      return reverseIntLookup[int]
    }
    return 7
  },
  rgbs: rgbStrings,
  intLookup: intLookup
}

export default colors
