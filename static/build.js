const stealTools = require("steal-tools")
const precache = require("steal-serviceworker")

stealTools.build({ config: __dirname + "/package.json!npm" },
                 {
                   bundleSteal: true 
                 }).then(
    function(buildResult){
        precache(buildResult, {
         staticFileGlobs: [ 
                "*.png",
                "dist/**/*.{css,js}",
                "node_modules/p5/lib/p5.min.js",
                "node_modules/@icon/open-iconic/open-iconic.css",
                "node_modules/xp.css/dist/ms_sans_serif_bold.woff2",
                "node_modules/@icon/open-iconic/open-iconic.woff"
            ]
        })
    });
