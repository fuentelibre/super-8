import { range, random, clamp, debounce } from 'lodash'
import { default as frameBufferCanvasAPI, injectHighlight } from './frameBufferCanvasAPI/index.js'
import getUserInput, { allowedKeys } from './getUserInput.js'
import pixelData from './frameBufferCanvasAPI/pixelData.js'
import colors from './colors.js'

console.log("*** PURE_GAME ***")

const INTRO_FRMS = 40

var GAME = "fire",
    CODE,
    CANVAS,
    SKETCH,
    STATE = {},
    VM_INIT,
    VM_DRAW,
    VM_UPDATE,
    P5BUFFER,
    PIX_DATA,
    LAST_FRAME,
    KEYS = new Set()

function sandbox(js) {
    let p5 = this.p5.createGraphics(200, 160)
    p5.loadPixels()
    let pixData = pixelData({ 'width': 200, 'height': 160 })

    let s8 = frameBufferCanvasAPI({
          'pixels': pixData.pixels,
          'ctx': CANVAS.getContext('2d'),
          'width': 200,
          'height': 160,
          'sprites': window.sprites,
          'map': window.maps
        })

    Object.assign(window, s8)
    Object.assign(window, {range, clamp, random})

    let init = ()=> {}
    let update = ()=> {}
    let draw = ()=> null
    eval(js)
    this.init = init
    this.update = update
    this.draw = draw
    this.p5buffer = p5
    this.pixData = pixData
    return this
}

function main() {
  CODE = _get(`/shelf/${GAME}/code`)
  let API = { 'p5': SKETCH }
  function eval_in_context(js, context) {
      const bound = sandbox.bind(context)
      return bound(js)
  }
  let inner = eval_in_context(CODE, API)
  console.log(inner)
  if (inner.init!=undefined) {
    VM_INIT = inner.init.bind(inner)
    console.log(VM_INIT)
  }

  VM_UPDATE = inner.update.bind(inner)
  VM_DRAW = inner.draw.bind(inner)
  P5BUFFER = inner.p5buffer
  PIX_DATA = inner.pixData
}

function setup(sketch) {
  sketch.noSmooth()
  sketch.pixelDensity(1)
  sketch.createCanvas(200,160)
  sketch.canvas.tabIndex = 0
  sketch.frameRate(30)
  sketch.strokeCap(p5.SQUARE)
  sketch.fill('#0000')
  let ctx = sketch.canvas.getContext("2d")
  ctx.globalAlpha = 1
  CANVAS = sketch.canvas
  SKETCH = sketch
  main()
}

function draw (sketch) {
    if (VM_INIT!=undefined && sketch.frameCount == INTRO_FRMS) {
        let newState = {}
        VM_INIT(newState)
        Object.assign(STATE, newState)
        sketch.background('#3e404b')
    }
    if (VM_DRAW!=undefined && sketch.frameCount > INTRO_FRMS) {
        let input = getUserInput(KEYS)
        if (VM_UPDATE!=undefined) {
            let elapsed = sketch.millis() - LAST_FRAME || 0
            STATE = VM_UPDATE(STATE, input, elapsed) || STATE
            LAST_FRAME = sketch.millis()
        }
        P5BUFFER.updatePixels()
        VM_DRAW(STATE)
        PIX_DATA.writePixelDataToCanvas(CANVAS.getContext('2d'))
        sketch.image(P5BUFFER, 0, 0)
    }
    else {
      drawIntro(sketch)
    }
}

function drawIntro(sketch) {
  sketch.push()
  sketch.background(0 + sketch.random(10))
  sketch.stroke(255)
  sketch.strokeWeight(2)
  sketch.fill(128)
  sketch.ellipse(100,70,50,50)
  sketch.noStroke()
  sketch.fill(128 + sketch.frameCount + sketch.random(100))
  sketch.arc(100,70,48,48,-sketch.HALF_PI,
          sketch.frameCount / INTRO_FRMS * sketch.TWO_PI - sketch.HALF_PI)
  sketch.textSize(18)
  sketch.textAlign(sketch.CENTER)
  sketch.text('SUPER 8', 100, 122.5)
  sketch.fill(0)
  sketch.textAlign(sketch.RIGHT)
  sketch.text(INTRO_FRMS - sketch.frameCount, 109.5, 76.5)
  sketch.pop()
}

function _get(yourUrl){
  let Httpreq = new XMLHttpRequest()
  Httpreq.open("GET", yourUrl, false)
  Httpreq.send(null)
  return Httpreq.responseText 
}

function done(sketch) {
  sketch.setup = ()=> setup(sketch),
  sketch.draw = ()=> draw(sketch)
}

new p5(done, 'p5sketch')

function keydownHandler(ev) {
    KEYS.add(ev.key)
}

function keyupHandler(ev) {
    KEYS.delete(ev.key)
}
document.addEventListener('keydown', keydownHandler)
document.addEventListener('keyup', keyupHandler)
