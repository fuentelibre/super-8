import "./node_modules/codemirror/addon/dialog/dialog.css"
import "./node_modules/codemirror/addon/lint/lint.css"
import "./node_modules/codemirror/lib/codemirror.css"
import "./node_modules/codemirror/theme/mdn-like.css"
import "./node_modules/codemirror/theme/darcula.css"
import "./node_modules/vuetify/dist/vuetify.min.css"

import Vuetify from 'vuetify/dist/vuetify.min.js'
import Vue from 'vue'
import VueCodemirror from 'vue-codemirror'
import VueShortKey from 'vue-shortkey'
import CodeMirror from 'codemirror'
import { JSHINT } from 'jshint'
import 'codemirror/mode/javascript/javascript.js'
import 'codemirror/addon/lint/lint.js'
import 'codemirror/addon/lint/javascript-lint.js'
import 'codemirror/addon/selection/active-line.js'
import 'codemirror/addon/dialog/dialog.js'
import 'codemirror/addon/edit/matchbrackets.js'
import 'codemirror/addon/edit/closebrackets.js'
import 'codemirror/addon/search/search.js'
import 'codemirror/addon/search/searchcursor.js'
import 'codemirror/addon/search/jump-to-line.js'
import 'codemirror/addon/search/match-highlighter.js'
import FS from '@isomorphic-git/lightning-fs'
import infLoopBuster from './InfiniteLoopBuster/app.js'
import StateMachine from 'javascript-state-machine'
import nipplejs from 'nipplejs'

import * as trace from 'trace.js'
import { resetTimer } from './InfiniteLoopBuster/stopExecutionOnTimeout.js'
import { random, clamp, range, debounce } from 'lodash'
import { default as frameBufferCanvasAPI, injectHighlight } from './frameBufferCanvasAPI/index.js'
import getUserInput, { allowedKeys } from './getUserInput.js'
import pixelData from './frameBufferCanvasAPI/pixelData.js'
import colors from './colors.js'

const empty_frame = range(8).map(()=>"        ")
window.empty_frame = empty_frame

const fs = new FS("browser-working-copy").promises
const fss = new FS("browser-working-copy")
window.fs = fss
window._ = { random, clamp, range, debounce }
window.frameBufferCanvasAPI = frameBufferCanvasAPI
window.colors = colors
window.getUserInput = getUserInput
window.allowedKeys = allowedKeys
window.pixelData = pixelData
window.infLoopBuster = infLoopBuster.handler
window.StateMachine = StateMachine
window.resetTimer = resetTimer
window.nipplejs = nipplejs
window.JSHINT = JSHINT

Vue.use(Vuetify)
Vue.use(VueCodemirror)
Vue.use(VueShortKey)

startUp(Vue, fs, trace.getEvaledErrorPosition, CodeMirror)
const root = new Vue({ el:"#super8",
	  vuetify: new Vuetify(),
          data: {
              dark: false,
              code: "",
              sprites: {},
              zoom: 1,
              map: range(80).map(()=>range(200).fill(null)),
              selectedSprite: 0,
              title: "",
              markLocation: null,
              editorPosition: null,
              isChangedCode: false,
              isChangedSprites: false,
              isChangedMap: false,
              sideBarWidth: null,
              currentTab: null
          }
	})
window.root = root

window.game_template = `
function init(state) {
  
}

function update(state, input, elapsed) {
  
}

function draw (state) {
  
}
`

if ( guy ) {
  guy.init(()=>{
    window.server=self
    guy.emitMe('server-ready')
  })
}

if ('serviceWorker' in navigator) {
  console.log('Installing Service Worker')
  navigator.serviceWorker.register('/service-worker.js')
}

