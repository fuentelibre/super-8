from guy import Guy, http


class Standalone(Guy):
    """ 
        <h1>Standalone Game Player</h1>
        
        <ul>
        <li>Reuse component</li>
        <li>New template.</li>
        <li>Include p5.js, nipple.js... input...</li>
        <li>Don't include any dev tools</li>
        </ul>

        <button onclick="self.test()">Lets go</button>
    """
    def __init__(self,txt='lala'):
        Guy.__init__(self)

    def test(self):
        self.par = self.parent.template
        print(self.parent.template)
