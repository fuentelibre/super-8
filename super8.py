#!/bin/env python3
""" SUPER 8 Fantasy Console
Copyright (c) Sebastian Silva - sebastian@fuentelibre.org

This software is under the AGPL-3 license.
"""
import os
import sys
from base64 import decodebytes
from datetime import datetime
import subprocess
from guy import Guy, http
import vbuild
from metapensiero.pj.api import translates
from ruamel import yaml
from markdown.extensions.wikilinks import WikiLinkExtension
from markdown import markdown
from p5_reference import referenceData

from standalone import Standalone

P5_BLACKLIST = ['p5.sound', 'Lights, Camera', 'Rendering', 'Structure', 'IO', 'DOM']

GAMES_PATH = './shelf/'



class Super8(Guy):
    """
    # Websocket Backend for Super-8

    Renders the Vue components and offers access to the Shelf
    """
    template = None

    def __init__(self, production=True):
        Super8.api = False
        if production:
            Super8.template = "static/template_production.html"
        else:
            Super8.template = "static/template.html"
        Guy.__init__(self)

    def render(self, includeGuyJs=False):
        vue = vbuild.render("static/*.vue")
        template = open(Super8.template).read()
        template = template.replace("/* SCRIPT */", str(vue.script))
        template = template.replace("/* STYLE */", str(vue.style))
        return template.replace("<!-- HTML -->", str(vue.html))

    async def get_mtime(self, title):
        """ Shelf action to load game data and code """
        full_path = os.path.join(GAMES_PATH, title)
        try:
            mtime = os.stat(full_path).st_mtime
        except FileNotFoundError:
            mtime = 0
        print('mtime for ' + title + ' is ' + str(mtime))
        return mtime

    async def read_file(self, title, filename):
        """ Shelf action to load game data and code """
        print('Loading ' + title + ': ' + filename)
        full_path = os.path.join(GAMES_PATH, title)
        with open(os.path.join(full_path, filename), 'r') as opened_file:
            return opened_file.read()

    async def write_file(self, title, filename, content):
        """ Shelf action to store game data and code """
        print('Saving ' + title + ': ' + filename)
        full_path = os.path.join(GAMES_PATH, title)
        os.makedirs(full_path, exist_ok=True)
        os.utime(full_path)
        with open(os.path.join(full_path, filename), 'w') as opened_file:
            opened_file.write(content)

    async def write_img(self, title, filename, content):
        """ Shelf action for base64 encoded previews """
        print('Saving ' + title + ': ' + filename)
        full_path = os.path.join(GAMES_PATH, title)
        os.makedirs(full_path, exist_ok=True)
        os.utime(full_path)
        content = decodebytes(bytes(content.split(',')[1], 'utf-8'))
        with open(os.path.join(full_path, filename), 'wb') as opened_file:
            opened_file.write(content)
        await self.emit('update-shelf')

    async def remove_game(self, title):
        """ Remove game directory contents - we don't really remove only hide the directory """
        full_path = os.path.join(GAMES_PATH, title)
        if os.path.exists(full_path) and os.path.isdir(full_path):
            timestamp = str(int(datetime.now().timestamp()))
            os.rename(full_path, os.path.join(GAMES_PATH, '._' + title + '_' + timestamp))
            await self.emit('update-shelf')

    async def get_list(self):
        """ Returns dict with shelf contents """
        return [{'title': f.name,
                 'mtime': datetime.utcfromtimestamp(f.stat().st_mtime)}
                for f in os.scandir(GAMES_PATH)
                if f.is_dir() and not f.name.startswith('.')]

    async def get_wiki(self, title):
        """ Returns html with wiki contents """
        if title=="s8api":
            # We render the Script-8 API docs
            full_path = "./apiDocs.yaml"
            def process(text):
                if not Super8.api:
                    Super8.api = yaml.safe_load(text)
                def make_section(index, section):
                    return [{'id': '%i,%i' % (index, index2),
                             'detail': markdown(item[1]),
                             'name': item[0] or 'Introduction'}
                             for index2,item in enumerate(section['dl'])]
                return [{'id': index,
                         'children': make_section(index, section),
                         'name': section['title']}
                         for index,section in enumerate(Super8.api)]
        elif title=="p5api":
            def make_items(index, index2, module):
                return [{'id': '%s,%s,%s' % (index, index2, index3),
                        'detail': ( (leaf.get('description') or '')
                                    + '\n<br><h2>Examples</h2>'
                                    + '<i>note: add <b>"p5."</b> prefix to all p5.js API calls (in Super&#8209;8).</i>'
                                    + '<div id="example">'
                                    + leaf.get('example')[0] if 'example' in leaf else '') + "</div>",
                        'name': ('p5.' if not leaf['module']=='Foundation' else '') + leaf.get('name')}
                        for index3,leaf in enumerate(
                            [item for item in referenceData['classitems']
                                if (item.get('module')==module or item.get('submodule')==module)
                                    and item.get('name')])]
            def make_section(index, sections):
                valid_submodules = {item.get('submodule'): True
                        for item in referenceData['classitems']
                        if item.get('name')}
                return [{'id': '%s,%s' % (index, index2),
                         'children': make_items(index, index2,
                             referenceData['modules'][item].get('name')),
                         'name': referenceData['modules'][item].get('name')}
                         for index2,item in enumerate(sections.keys())
                         if item in valid_submodules.keys()]
            return [{'id': str(idx),
                'detail': referenceData['modules'][module].get('description'),
                'children': make_section(idx,
                            referenceData['modules'][module].get('submodules')) or
                            make_items(idx, module, module),
                'name': referenceData['modules'][module]['name']}
                for idx, module in enumerate(referenceData['modules'])
                if not referenceData['modules'][module].get('is_submodule') and
                not module in P5_BLACKLIST]
        else:
            full_path = os.path.join('wiki', title + '.md')
            if title=='README':
                full_path = 'README.md'
            if title=='AGPL':
                full_path = 'COPYING'
            wiki_link = WikiLinkExtension(base_url='#w=', end_url='')
            clickevent = '{: onclick="window.aclick(event)" }'
            process = lambda text: markdown(text.replace(']]', ']]' + clickevent), 
                    extensions=[wiki_link, 'attr_list', 'md_in_html'])
            if title=='AGPL':
                process = lambda text: '<code>'+text+'</code>'
        with open(full_path, 'r') as opened_file:
            return process(opened_file.read())

    @http("/preview/(.+)")
    def get_preview(web, name):  # pylint: disable=no-self-argument
        """ Provides http image previews """
        # pylint: disable=no-member
        web.set_header("Content-Type", "image/png")
        try:
            file = open(os.path.join(GAMES_PATH, name, "preview.png"), "rb")
        except FileNotFoundError:
            file = open(os.path.join(GAMES_PATH, "stub.png"), "rb")
        try:
            web.write(file.read())  # pylint: disable=no-member
        finally:
            file.close()

    @http("/gitlab-webhook/(.+)")
    def get_hook(web, name):  # pylint: disable=no-self-argument
        """ Provides transpiled files"""
        web.set_header('Content-Type', 'application/json;')
        web.write('{"status": "ok"}')
        print('Committing shelf games')        
        subprocess.run(['git', 'add', 'shelf/*'])
        subprocess.run(['git', 'commit', '-a', '-m', "Add local changes"])
        print('Pulling new version')        
        subprocess.run(['git', 'pull', '--rebase', '--autostash', 
                        '--strategy', 'recursive-theirs'])
        print('Exiting!')        
        sys.exit()

    @http("/pj/(.+)")
    def get_pj(web, name):  # pylint: disable=no-self-argument
        """ Provides transpiled files"""
        web.set_header('Content-Type', 'application/javascript;')
        try:
            with open(os.path.join('./pj/', name + '.py'), 'r') as opened_file:
                web.write(translates(opened_file.read(),
                                     enable_es6=True)[0])
        except FileNotFoundError:
            web.set_status(404)

    @http("/shelf/(.+)/(.+)")
    def get_game(web, game, item):  # pylint: disable=no-self-argument
        """ Provides http access to game data """
        # pylint: disable=no-member
        if item=='code':
            filename = 'code.js'
            web.set_header('Content-Type', 'application/javascript;')
        if item=='sprites':
            filename = 'sprites.json'
            web.set_header('Content-Type', 'application/json;')
        if item=='map':
            filename = 'map.json'
            web.set_header('Content-Type', 'application/json;')
        file = open(os.path.join(GAMES_PATH, game, filename), 'rb')
        try:
            web.write(file.read())  # pylint: disable=no-member
        finally:
            file.close()


if __name__ == "__main__":
    # Launch the Super-8 Guy App
    S8 = Super8()
    S8.run()
